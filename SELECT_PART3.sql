-- Parte 3
-- a) Apresentar por andar, o quarto e o tipo de quarto, que teve o maior número de
-- reservas. Deverão ser excluídos todos os quartos em que o número de reservas
-- é inferior a 2 e são do tipo “single�?. Não incluir reservas canceladas.
select numero_andar, numero_quarto, tipoquarto.designacao, count(*) as total from reserva r
inner join tipoquarto on tipoquarto.id_tipo_quarto = r.id_tipo_quarto
where 
    numero_andar is not null 
    and r.id_tipo_quarto not in (
        select id_tipo_quarto from tipoquarto
        where designacao like 'Single'
    )       
group by numero_andar, numero_quarto, tipoquarto.designacao
having count(*) > 1 and count(*) = (
    select max(count(*)) from reserva r1
    inner join tipoquarto tq on r1.id_tipo_quarto = tq.id_tipo_quarto
    where 
        r1.numero_andar = r.numero_andar
        and r1.numero_quarto is not null
        and tq.designacao not like 'Single'
    group by numero_andar, numero_quarto
)
order by numero_andar; 

-- b) Apresentar os clientes que ocuparam quartos do tipo suite na última época alta
-- e consumiram os dois produtos com maior consumo nos últimos dois anos. O
-- resultado deve ser ordenado por ordem decrescente do valor total do consumo.

with

ultima_epoca_alta as (

    -- Traduzir a data das epocas para o ano atual e verificar se j� est� no 
    -- passado para ser usada essa ou a do ano anterior a essa
    select (case when
        add_months(data_inicio, (extract(YEAR from sysdate) - extract(YEAR from data_inicio)) * 12) > sysdate then add_months(data_inicio, (extract(YEAR from sysdate) - extract(YEAR from data_inicio) - 1) * 12)
        else  add_months(data_inicio, (extract(YEAR from sysdate) - extract(YEAR from data_inicio)) * 12)
        end) as data_inicio,
        (case when add_months(data_fim, (extract(YEAR from sysdate) - extract(YEAR from data_fim)) * 12) > sysdate then add_months(data_fim, (extract(YEAR from sysdate) - extract(YEAR from data_fim) - 1) * 12)
        else  add_months(data_fim, (extract(YEAR from sysdate) - extract(YEAR from data_fim)) * 12)
        end) as data_fim
    from epoca
    where designacao like 'Alta'
),
produtos_mais_consumidos as (
    select id_produto from consumo
    where consumo.data_consumo > sysdate - interval '2' year
    group by id_produto
    order by count(*) desc
    fetch first 2 rows only
)

select nif, sum(total) as total from (
    -- Clientes que consumiram ambos os produtos mais vendidos dos �ltimos dois anos
    select r.nif, c.id_produto, sum(c.quantidade) as total from consumo c
    inner join contaconsumo cc on cc.id_conta = c.id_conta
    inner join reserva r on r.id_reserva = cc.id_reserva
    right outer join produtos_mais_consumidos p on p.id_produto = c.id_produto
    where r.nif is not null
    group by r.nif, c.id_produto
    order by total
)
where nif in (
    -- Clientes que ocuparam quartos do tipo suite na �ltima �poca alta
    select nif from reserva, tipoquarto
    where 
        reserva.id_tipo_quarto = tipoquarto.id_tipo_quarto
        and tipoquarto.designacao like 'Suite'
        and data_inicio BETWEEN (select data_inicio from ultima_epoca_alta) and (select data_fim from ultima_epoca_alta)
)
group by nif
having count(*) = (select count(*) from produtos_mais_consumidos)
;
