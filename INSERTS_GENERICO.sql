INSERT INTO Andar(numero_andar, nome) VALUES (?, ?);

INSERT INTO Camareira(nif_funcionario) VALUES (?);

INSERT INTO Cliente(nome, nif, email, telefone) VALUES (?, ?, ?, ?);

INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) VALUES (?, ?, ?, ?, ?);

INSERT INTO ContaConsumo(data_abertura, id_conta, id_reserva) VALUES (?, ?, ?);

INSERT INTO Epoca(designacao, data_inicio, data_fim) VALUES (?, ?, ?);

INSERT INTO Estado(id_estado, descricao_estado) VALUES (?, ?);

INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (?, ?, ?);

INSERT INTO Fatura(id_fatura, valor_total, id_fatura_consumo, id_fatura_reserva) VALUES (?, ?, ?, ?);

INSERT INTO Fatura_MetodoPagamento(id_fatura, id_metodo_pagamento, valor_pago) VALUES (?, ?, ?);

INSERT INTO FaturaConsumo(id_fatura_consumo, id_conta, valor) VALUES (?, ?, ?);

INSERT INTO FaturaReserva(id_fatura_reserva, id_reserva, valor) VALUES (?, ?, ?);

INSERT INTO Funcionarios(nif, nome, telefone, email) VALUES (?, ?, ?, ?);

INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (?, ?);

INSERT INTO IntervencaoManutencao(id_intervencao, nif_funcionario, descricao, id_pedido_intervencao) VALUES (?, ?, ?, ?);

INSERT INTO Manutencao(nif_funcionario, telefone_servico, id_responsavel) VALUES (?, ?, ?);

INSERT INTO MetodoPagamento(id_metodo_pagamento, descricao) VALUES (?, ?);

INSERT INTO Morada(nif, rua, numero, localidade) VALUES (?, ?, ?, ?);

INSERT INTO PedidoIntervencao(id_pedido_intervencao, data_pedido_intervencao, id_reserva) VALUES (?, ?, ?);

INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (?, ?, ?, ?);

INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (?, ?, ?, ?);

INSERT INTO Rececao(nif_funcionario) VALUES (?);

INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) VALUES (?, ?, ?, ?);

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) VALUES (?, ?, ?, ?, ?, ?, ?, ?);

INSERT INTO Restaurante(nif_funcionario) VALUES (?);

INSERT INTO TipoProduto(id_tipo_produto, descricao_tipo) VALUES (?, ?);

INSERT INTO TipoQuarto(id_tipo_quarto, designacao) VALUES (?, ?);

INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES (?, ?, ?);
