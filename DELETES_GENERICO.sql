DELETE FROM Andar 
  WHERE numero_andar = ?;

DELETE FROM Camareira 
  WHERE nif_funcionario = ?;

DELETE FROM Cliente 
  WHERE nif = ?;

DELETE FROM Consumo 
  WHERE id_conta = ? AND data_consumo = ? AND id_produto = ?;

DELETE FROM ContaConsumo 
  WHERE id_conta = ?;

DELETE FROM Epoca 
  WHERE designacao = ?;

DELETE FROM Estado 
  WHERE id_estado = ?;

DELETE FROM EstadoReserva 
  WHERE id_reserva = ? AND id_estado = ? AND data = ?;

DELETE FROM Fatura 
  WHERE id_fatura = ?;

DELETE FROM Fatura_MetodoPagamento 
  WHERE id_fatura = ? AND id_metodo_pagamento = ?;

DELETE FROM FaturaConsumo 
  WHERE id_fatura_consumo = ?;

DELETE FROM FaturaReserva 
  WHERE id_fatura_reserva = ?;

DELETE FROM Funcionarios 
  WHERE nif = ?;

DELETE FROM IntervencaoLimpeza 
  WHERE id_intervencao = ?;

DELETE FROM IntervencaoManutencao 
  WHERE id_intervencao = ?;

DELETE FROM Manutencao 
  WHERE nif_funcionario = ?;

DELETE FROM MetodoPagamento 
  WHERE id_metodo_pagamento = ?;

DELETE FROM Morada 
  WHERE nif = ?;

DELETE FROM PedidoIntervencao 
  WHERE id_pedido_intervencao = ?;

DELETE FROM Produto 
  WHERE id_produto = ?;

DELETE FROM Quarto 
  WHERE numero_quarto = ? AND numero_andar = ?;

DELETE FROM Rececao 
  WHERE nif_funcionario = ?;

DELETE FROM RegistoIntervencao 
  WHERE id_intervencao = ?;

DELETE FROM Reserva 
  WHERE id_reserva = ?;

DELETE FROM Restaurante 
  WHERE nif_funcionario = ?;

DELETE FROM TipoProduto 
  WHERE id_tipo_produto = ?;

DELETE FROM TipoQuarto 
  WHERE id_tipo_quarto = ?;

DELETE FROM TipoQuarto_Epoca 
  WHERE designacao = ? AND id_tipo_quarto = ?;