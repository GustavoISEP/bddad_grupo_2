--PARTE1

--A
/*Apresentar todos os pedidos de intervenção em aberto (intervenção ainda não
registada) alocados a funcionários de manutenção que não fizeram nenhuma
intervenção (registo) nas últimas 48 hora;*/

/*SELECT PI.*
FROM pedidointervencao PI
INNER JOIN intervencaomanutencao IM ON IM.id_pedido_intervencao = PI.id_pedido_intervencao
INNER JOIN registointervencao RI ON RI.id_intervencao = IM.id_intervencao 
WHERE RI.data_intervencao IS NULL;*/

SELECT PI.*
FROM pedidointervencao PI
INNER JOIN intervencaomanutencao IM ON IM.id_pedido_intervencao = PI.id_pedido_intervencao
INNER JOIN registointervencao RI ON RI.id_intervencao = IM.id_intervencao
WHERE RI.data_intervencao IS NULL AND IM.nif_funcionario NOT IN
(SELECT IM.nif_funcionario
FROM intervencaomanutencao IM
INNER JOIN registointervencao RI ON RI.id_intervencao = IM.id_intervencao
WHERE ROUND((24 * ((select sysdate from dual) - CAST(RI.data_intervencao AS DATE))), 0) < 48);

--B 
/*Apresentar a data, a hora e o nome dos clientes que reservaram quartos
somente durante o mês de Abril e Junho deste ano. No caso de algum cliente ter
reservado um quarto do tipo suite, deverá apresentar a localidade desse cliente
numa coluna intitulada “Zona do País”. O resultado deverá ser apresentado por
ordem alfabética do nome de cliente e por ordem descendente da data e hora da
reserva*/

/*SELECT DISTINCT C.nome, ER.data, (case when TQ.designacao = 'Suite' then L.descricao_localidade else '' end) ZonaPais 
FROM Cliente C, reserva R, estadoreserva ER, estado R, Tipoquarto TQ, Localidade L
WHERE R.nif = C.nif AND R.id_reserva = er.id_reserva AND C.id_localidade = L.id_localidade AND tq.id_tipo_quarto = R.id_tipo_quarto AND er.id_estado = 1 AND EXTRACT(YEAR FROM ER.data) = 2020 AND (EXTRACT(MONTH FROM ER.data) = 4 OR EXTRACT(MONTH FROM ER.data) = 6);*/

SELECT C.nome, ER.data, (case when TQ.designacao = 'Suite' then L.descricao_localidade else '' end) "Zona do País" 
FROM Cliente C
INNER JOIN reserva R ON C.nif = R.nif 
INNER JOIN estadoreserva ER ON R.id_reserva = ER.id_reserva
INNER JOIN localidade L ON C.id_localidade = L.id_localidade
INNER JOIN Tipoquarto TQ ON R.id_tipo_quarto = TQ.id_tipo_quarto
WHERE ER.id_estado = 1 AND EXTRACT(YEAR FROM ER.data) = 2020 AND (EXTRACT(MONTH FROM ER.data) = 4 OR EXTRACT(MONTH FROM ER.data) = 6)
ORDER BY C.nome ASC, ER.data DESC;