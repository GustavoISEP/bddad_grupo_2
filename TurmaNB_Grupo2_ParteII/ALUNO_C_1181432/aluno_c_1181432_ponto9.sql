create or replace trigger trgAtualizaCliente
after insert or update on cliente
for each row
declare
    res reserva%rowtype;
    
    cursor c_res(cli_nome cliente.nome%type) is
        select * from reserva
        where nome = cli_nome
        for update;
begin
    open c_res(:new.nome);
    loop
        fetch c_res into res;
        exit when c_res%notfound;
        update reserva
        set id_cliente = :new.id, nif = :new.nif, email = :new.email, telefone = :new.telefone
        where id = res.id;
    end loop;
end;


set serveroutput on;
declare
    -- Dados de cliente
    cli_nome cliente.nome%type := 'Cliente B';
    cli_id cliente.id%type := 2000;
    cli_nif cliente.nif%type := 123456789;
    cli_email cliente.email%type := 'teste@test.eu';
    cli_tel cliente.telefone%type := 918884848;
    
    -- Dados de reserva
    tq tipo_quarto.id%type := 1;
    data_entrada date := to_date('2021-04-20', 'yyyy-mm-dd');
    data_saida date := to_date('2021-04-22', 'yyyy-mm-dd');
    num_pessoas int := 2;
    
    id_reserva reserva.id%type;
    res reserva%rowtype;
    cli cliente%rowtype;
begin
    -- Criar reserva com nome de cliente para posterior emparelhamento.
    prcRegistarReserva(tq, data_entrada, data_saida, num_pessoas, null, cli_nome);
    
    select max(id) into id_reserva from reserva;
    select * into res from reserva where id = id_reserva;
    dbms_output.put_line('Antes de trigger - reserva id_cliente: '|| res.id_cliente || ', reserva nome: '|| res.nome || ', reserva telefone: '|| res.telefone || ', reserva email: '|| res.email);
    
    -- Criar utilizador para despoletar trigger
    insert into cliente(id, nome, nif, id_localidade, email, telefone)
    values (cli_id, cli_nome, cli_nif, null, cli_email, cli_tel);
    select * into cli from cliente where id = cli_id;
    dbms_output.put_line('Criado utilizador: id: '|| cli.id ||', nome: '|| cli.nome ||', nif: '|| cli.nif ||', telefone: '|| cli.telefone);
    
    -- Dados de cliente adicionados a reserva ap�s trigger.
    select * into res from reserva where id = id_reserva;
    dbms_output.put_line('Depois de trigger - reserva id_cliente: '|| res.id_cliente || ', reserva nome: '|| res.nome || ', reserva telefone: '|| res.telefone || ', reserva email: '|| res.email);
    
    -- Limpar dados
    delete from reserva where id_cliente = 2000;
    delete from cliente where id = cli_id;
end;


