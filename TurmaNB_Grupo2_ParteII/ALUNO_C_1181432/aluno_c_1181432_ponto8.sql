-- Funcoes auxiliares
create or replace function fncEpocadeData (data_atual date) return epoca%rowtype
is
    epoca_row epoca%rowtype;
    epoca_data_ini date;
    epoca_data_fim date;
    diff_meses int;
    
    cursor c_epocas is
        select * from epoca;
begin
    open c_epocas;
    loop
        fetch c_epocas into epoca_row;
        exit when c_epocas%notfound;
        diff_meses := (extract(year from data_atual) - extract(year from epoca_row.data_ini)) * 12;
        epoca_data_ini := add_months(epoca_row.data_ini, diff_meses);
        epoca_data_fim := add_months(epoca_row.data_fim, diff_meses);
        if epoca_data_ini <= data_atual and epoca_data_fim >= data_atual then
            return epoca_row;
        end if;
    end loop;
    return null;
end;

create or replace function fncCalculaPreco(
    data_inicio date, 
    data_fim date, 
    tq tipo_quarto.id%type
) return number
is
    preco number(6, 2) := 0;
    inicio number;
    fim number;
    dia_atual date;
    epoca_atual epoca%rowtype;
    preco_dia number(6, 2);
   
begin
    inicio := to_number(to_char(data_inicio, 'j'));
    fim := to_number(to_char(data_fim, 'j'));
    
    for dia in inicio..fim loop
        dia_atual := to_date(dia, 'j');
        epoca_atual := fncEpocadeData(dia_atual);
        
        select preco into preco_dia from preco_epoca_tipo_quarto 
        where id_tipo_quarto = tq and id_epoca = epoca_atual.id;
        
        preco := preco + preco_dia;
    end loop;
    
    dbms_output.put_line('O preco para '|| (fim - inicio) || ' dias: ' || preco || '�');
    return preco;
end;


create or replace procedure prcRegistarReserva(
    tq tipo_quarto.id%type,
    data_entrada date,
    data_saida date,
    num_pessoas int,
    id_cliente cliente.id%type := null,
    nome_cliente cliente.nome%type := null,
    nif_cliente cliente.nif%type := null,
    tel_cliente cliente.telefone%type := null,
    email_cliente cliente.email%type := null
)
is
    duracao int;
    cli cliente%rowtype;
    cli_count int;
    preco number(6, 2);
    estado_inicial estado_reserva.id%type := 1;
    id_reserva reserva.id%type;
    
    -- Excepcoes
    campos_id_nao_nulos exception;
    nome_cliente_nulo exception;
    reserva_nao_possivel exception;
    datas_incompativeis exception;
begin
    dbms_output.put_line('A registar reserva...');
    
    -- Validar as datas introduzidas
    if data_entrada >= data_saida then
        raise datas_incompativeis;
    end if;
    
    -- Validar parametros de cliente e ir buscar instancia de cliente associado.
    if id_cliente is not null then
        if null not in (nome_cliente, nif_cliente, tel_cliente, email_cliente) then
            raise campos_id_nao_nulos;
        end if;
        select * into cli from cliente where id = id_cliente;
    elsif nome_cliente is null then
        raise nome_cliente_nulo;
    else
        -- Verificar se existe cliente com o nome passado como parametro.
        -- Caso nao exista preenche-se os dados da reserva com os dados
        -- passados como parametro para mais tarde preencher com os dados de cliente.
         select count(*) into cli_count from cliente where nome = nome_cliente;
        if cli_count = 1 then
            select * into cli from cliente where nome = nome_cliente;
        else
            cli.nome := nome_cliente;
            cli.nif := nif_cliente;
            cli.email := email_cliente;
            cli.telefone := tel_cliente;
        end if;
    end if;
    
    if cli.id is not null then
        dbms_output.put_line('Foi encontrado o cliente '|| cli.id ||' com o nome '|| cli.nome);
    else
        dbms_output.put_line('Nao foi encontrado cliente com o nome'|| cli.nome || '. A guardar dados...');
    end if;
    -- Verificar se a reserva � possivel
    -- Como a subtracao nao tem em conta o dia de entrada adicionamos 1 dia.
    duracao := (data_saida - data_entrada) + 1;
    if not fncDisponibilidadeReserva(tq, data_entrada, duracao, num_pessoas) then
        raise reserva_nao_possivel;
    end if;
    
    -- Calcular o preco com base nas datas.
    preco := fncCalculaPreco(data_entrada, data_saida, tq);
    
    dbms_output.put_line('Verificacoes terminadas - Pronto a registar.');
    select max(id) + 1 into id_reserva from reserva;  -- Gerar id.
    insert into reserva(
                id, id_cliente, nome, nif, email, 
                telefone, id_tipo_quarto, data, 
                data_entrada, data_saida, nr_pessoas, 
                preco, id_estado_reserva, custo_cancelamento, custo_extra)
    values (
        id_reserva, cli.id, cli.nome, cli.nif, cli.email, cli.telefone, 
        tq, sysdate, data_entrada, data_saida, num_pessoas, preco, 
        estado_inicial, 0, 0);
    
    dbms_output.put_line('Criada reserva '|| id_reserva);
    
exception
    when campos_id_nao_nulos then
        RAISE_APPLICATION_ERROR(-20002, 'Nome, nif, tel e email devem ser nulos quando dado id de cliente');
    when nome_cliente_nulo then
        RAISE_APPLICATION_ERROR(-20003, 'Nome nao pode ser nulo quando id nao e dado.');
    when reserva_nao_possivel then
        RAISE_APPLICATION_ERROR(-20004, 'A reserva nao e possivel.');
    when datas_incompativeis then
        RAISE_APPLICATION_ERROR(-20005, 'As datas introduzidas nao sao compativeis.');
end prcRegistarReserva;





-- Anon blocks for running 'prcRegistarReserva' procedure.

-- Registar reserva para um cliente atrav�s de id.
set serveroutput on;
declare
    tq tipo_quarto.id%type := 1;
    data_entrada date := to_date('2021-01-03', 'yyyy-mm-dd');
    data_saida date := to_date('2021-01-05', 'yyyy-mm-dd');
    num_pessoas int := 2;
    id_cliente int := 15;
begin
    prcRegistarReserva(tq, data_entrada, data_saida, num_pessoas, id_cliente);
end;

-- Registar reserva para um cliente atrav�s de nome.
set serveroutput on;
declare
    tq tipo_quarto.id%type := 1;
    data_entrada date := to_date('2021-04-07', 'yyyy-mm-dd');
    data_saida date := to_date('2021-04-09', 'yyyy-mm-dd');
    num_pessoas int := 2;
    nome_cliente varchar(20) := 'Cliente 14';
begin
    prcRegistarReserva(tq, data_entrada, data_saida, num_pessoas, null, nome_cliente);
end;

-- Registar reserva para um cliente inexistente atrav�s de nome.
set serveroutput on;
declare
    tq tipo_quarto.id%type := 1;
    data_entrada date := to_date('2021-04-07', 'yyyy-mm-dd');
    data_saida date := to_date('2021-04-09', 'yyyy-mm-dd');
    num_pessoas int := 2;
    nome_cliente varchar(20) := 'Cliente C';
begin
    prcRegistarReserva(tq, data_entrada, data_saida, num_pessoas, null, nome_cliente);
end;

-- Anon block for running 'prcRegistarReserva' procedure.
-- Falha no registo devido a id e nome nulo
set serveroutput on;
declare
    tq tipo_quarto.id%type := 1;
    data_entrada date := to_date('2021-01-01', 'yyyy-mm-dd');
    data_saida date := to_date('2021-01-03', 'yyyy-mm-dd');
    num_pessoas int := 2;
begin
    prcRegistarReserva(tq, data_entrada, data_saida, num_pessoas);
end;