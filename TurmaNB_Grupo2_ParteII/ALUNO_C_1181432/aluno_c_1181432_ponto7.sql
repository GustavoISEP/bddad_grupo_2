-- Retorna n�mero de quartos existentes para o tipo de quarto e lotacao maxima 
-- passados como argumento.
create or replace function fncNumQuartosParaTipoeLotacao(
    tq quarto.id_tipo_quarto%type, 
    lotacao quarto.lotacao_maxima%type
) return int
is
    r_count int;
begin
    select count(*) into r_count from quarto
    where id_tipo_quarto = tq and lotacao_maxima >= lotacao;
    return r_count;
end;


-- Verifica se o tipo de quarto dado � v�lido.
create or replace function fncTipodeQuartoValido(tq tipo_quarto.id%type) return boolean
is 
    tq_count int;
begin
    select count(*) into tq_count from tipo_quarto 
    where id = tq;
    return tq_count = 1;
end;

-- Retorna o número de reservas para um dado dia e tipo de quarto.
create or replace function fncReservasDiaparaTipo(dia date, tq tipo_quarto.id%type) return int
is
    res_count int;
begin
    select count(*) into res_count from reserva
    inner join tipo_quarto on reserva.id_tipo_quarto = tipo_quarto.id
    where 
        reserva.id_tipo_quarto = tq
        and reserva.data_entrada <= dia
        and reserva.data_saida > dia;
    return res_count;
end;

-- Funcao para exercicio
create or replace function fncDisponibilidadeReserva(
    tq tipo_quarto.id%type, 
    data_inicio date,
    duracao int,
    num_pessoas int
) return boolean
is
    res_count int;
    r_count int;
    duracao_real int;
    inicio number;
    fim number;
    dia_atual varchar2(10);
    
    data_passada exception;
    tipo_quarto_nao_valido exception;
    quartos_inexistentes exception;
    
begin
    -- Validar se o tipo de quarto existe
    if not fncTipodeQuartoValido(tq) then
        raise tipo_quarto_nao_valido;
    end if;
    
    -- Validar se a data pretendida para a reserva est� no futuro.
    if data_inicio <= sysdate then
        raise data_passada;
    end if;
    
    -- Verificar o numero de quartos m�ximos disponiveis para o tipo pretendido.
    r_count := fncNumQuartosParaTipoeLotacao(tq, num_pessoas);
    dbms_output.put_line('N�mero de quartos existentes para o tipo '|| tq ||' e lotacao de '|| num_pessoas ||': '|| r_count);
    if r_count = 0 then
        raise quartos_inexistentes;
    end if;
    
    -- Como o primeiro dia da estadia conta para a duracao, retiramos 1 dia.
    duracao_real := duracao - 1;
    
    inicio := to_number(to_char(data_inicio, 'j'));
    fim := to_number(to_char(data_inicio + duracao_real, 'j'));
    
    for dia in inicio..fim loop
        res_count := fncReservasDiaparaTipo(to_date(dia, 'j'), tq);
        dbms_output.put_line('N�mero de reservas existentes para '|| to_date(dia, 'j') ||': '|| res_count);
        
        -- Se o numero de reservas para um certo dia for igual (ou superior?) ao 
        -- numero de quartos disponiveis para o tipo pretendido entao nao e possivel 
        -- efetuar a reserva.
        if res_count >= r_count then
            dbms_output.put_line('Nao e possivel efetuar reserva.');
            return false;
        end if;
    end loop;
    
    dbms_output.put_line('E possivel efetuar a reserva.');
    return true;
    
exception 
    when data_passada then
        dbms_output.put_line('A data de entrada deve ser inferior � data atual');
        return null;
    when tipo_quarto_nao_valido then
        dbms_output.put_line('O tipo de quarto '|| tq || ' n�o � v�lido.');  
        return null;
    when quartos_inexistentes then
        dbms_output.put_line('Nao existem quartos para '|| num_pessoas || ' pessoas.');
        return null;
end;
    
    
    
    
--- Testes atraves de blocos anonimos
set serveroutput on;
declare
    tq tipo_quarto.id%type := 1;
    data_inicio date := to_date('2021-01-01', 'yyyy-mm-dd');
    duracao int := 2;
    num_pessoas int := 2;
begin
    
    if fncDisponibilidadeReserva(tq, data_inicio, duracao, num_pessoas) then
        dbms_output.put_line('Reserva pode ser efetuada');
    else
        dbms_output.put_line('Reserva nao pode ser efetuada');
    end if;    
end;


-- Teste data no passado.
-- Reserva efetuada para data valida
set serveroutput on;
declare
    tq tipo_quarto.id%type := 1;
    data_inicio date := to_date('2020-01-02', 'yyyy-mm-dd');
    duracao int := 1;
    num_pessoas int := 2;
begin
    
    if fncDisponibilidadeReserva(tq, data_inicio, duracao, num_pessoas) then
        dbms_output.put_line('Reserva pode ser efetuada');
    else
        dbms_output.put_line('Reserva nao pode ser efetuada');
    end if;    
end;


-- Teste quarto para numero de pessoas superior a existente.
set serveroutput on;
declare
    tq tipo_quarto.id%type := 1;
    data_inicio date := to_date('2021-01-25', 'yyyy-mm-dd');
    duracao int := 2;
    num_pessoas int := 5;
begin
    
    if fncDisponibilidadeReserva(tq, data_inicio, duracao, num_pessoas) then
        dbms_output.put_line('Reserva pode ser efetuada');
    else
        dbms_output.put_line('Reserva nao pode ser efetuada');
    end if;    
end;
