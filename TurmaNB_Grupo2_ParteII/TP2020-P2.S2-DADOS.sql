--sequences
--select * from user_sequences;
declare
  v_cmd varchar(2000);
begin
  for r in (select sequence_name from user_sequences)
  loop
    v_cmd := 'drop sequence ' || r.sequence_name;
    execute immediate(v_cmd);
  end loop;
  --
  for r in (select table_name from user_tables)
  loop
    v_cmd := 'create sequence seq_' || r.table_name;
    execute immediate(v_cmd);
  end loop;
end;
/

--Pa�ses
declare
  v_count int := 3;
  v_label varchar(50) := 'Pa�s';
begin
  for i in 1..v_count loop
    insert into pais(id, nome) values(seq_pais.nextval, v_label || ' ' || i);
  end loop;
end;
/

--Distritos
declare
  v_count int := 5;
  v_label varchar(50) := 'Distrito';
begin
  for r in (select id, nome from pais) loop
    for i in 1..v_count loop
      insert into distrito(id, id_pais, nome) values(seq_distrito.nextval, r.id, v_label || ' ' || i || ' ' || r.nome);
    end loop;
  end loop;
end;
/

--Concelhos
declare
  v_count int := 5;
  v_label varchar(50) := 'Concelho';
begin
  for r in (select id, nome from distrito) loop
    for i in 1..v_count loop
      insert into concelho(id, id_distrito, nome) values(seq_concelho.nextval, r.id, v_label || ' ' || i || ' ' || r.nome);
    end loop;
  end loop;
end;
/

--Localidades
declare
  v_count int := 5;
  v_label varchar(50) := 'Localidade';
begin
  for r in (select id, nome from concelho) loop
    for i in 1..v_count loop
      insert into localidade(id, id_concelho, nome) values(seq_localidade.nextval, r.id, v_label || ' ' || i || ' ' || r.nome);
    end loop;
  end loop;
end;
/

--C�digos postais
declare
  v_count int := 5;
  v_label varchar(20) := 'C�digo Postal';
begin
  for r in (select id, nome from localidade) loop
    for i in 1..v_count loop
      insert into codigo_postal(id, id_localidade, designacao_postal) values(seq_codigo_postal.nextval, r.id, v_label || ' ' || i || ' ' || r.nome);
    end loop;
  end loop;
end;
/

--Andares
declare
  v_count int := 3;
  v_label varchar(50) := 'Andar';
begin
  for i in 1..v_count loop
    insert into andar(id, nr_andar, nome) values(seq_andar.nextval, i, v_label || ' ' || i);
  end loop;
end;
/

--Tipos de quarto
declare
  v_count int := 3;
  v_label varchar(50) := 'Tipo quarto';
begin
  for i in 1..v_count loop
    insert into tipo_quarto(id, nome) values(seq_tipo_quarto.nextval, v_label || ' ' || i);
  end loop;
end;
/

--Quartos
declare
  v_count int := 7;
  v_label varchar(20) := 'Quarto';
  cursor c_tq is select id from tipo_quarto order by 1;
  r_tq c_tq%rowtype;
  v_id_tipo_quarto tipo_quarto.id%type;
  v_lotacao int;
begin
  for r in (select id, nome from andar) loop
    for i in 1..v_count loop
      if not c_tq%isopen then
        open c_tq;
      end if;
      fetch c_tq into r_tq;
      if c_tq%found then
        v_id_tipo_quarto := r_tq.id;
      else
        close c_tq;
        open c_tq;
        fetch c_tq into r_tq;
        v_id_tipo_quarto := r_tq.id;
      end if;
      --
      v_lotacao := mod(r.id * i, 3) + 2;
      insert into quarto(id, id_andar, nr_quarto, id_tipo_quarto, lotacao_maxima)
      values(seq_quarto.nextval, r.id, i, v_id_tipo_quarto, v_lotacao);
    end loop;
  end loop;
end;
/

--Clientes
declare
  v_count int := 500;
  v_label varchar(50) := 'Cliente';
  cursor c is select id from localidade order by 1;
  r c%rowtype;
begin
  for i in 1..v_count loop
    if not c%isopen then
      open c;
    end if;
    fetch c into r;
    if not c%found then
      close c;
      open c;
      fetch c into r;
    end if;
    --
    insert into cliente(id, nome, nif, id_localidade, email, telefone)
    values(seq_cliente.nextval, v_label || ' ' || i, i, r.id, null, null);
  end loop;
end;
/

--Tipos de funcion�rios
insert into tipo_funcionario(id, nome) values(1, 'Manuten��o');
insert into tipo_funcionario(id, nome) values(2, 'Camareira');
insert into tipo_funcionario(id, nome) values(3, 'Bar');
insert into tipo_funcionario(id, nome) values(4, 'Rece��o');

--Funcion�rios
declare
  v_count int := 10;
  v_label varchar(50) := 'Funcion�rio';
begin
  for r in (select * from tipo_funcionario order by 1) loop
    for i in 1..v_count loop
      insert into funcionario(id, nif, nome, id_tipo_funcionario) values(seq_funcionario.nextval, seq_funcionario.currval, r.nome || ' ' || i, r.id);
    end loop;
  end loop;
end;
/

--Funcion�rios de manuten��o.
declare
  v_id_responsavel funcionario.id%type;
begin
  for r in (select * from funcionario where id_tipo_funcionario = 1) loop
    insert into funcionario_manutencao(id, id_responsavel, telefone) values(r.id, v_id_responsavel, r.id);
    v_id_responsavel := r.id;
  end loop;
end;
/

--- BEGIN -- 1081699 - Tiago Monteiro - Altera��es para exercisios Aluno B do trabalho 2

-- carregar id_bonus_camareira. Neste caso o id da camareira vai ser igual ao do seu bonus. Aproveitei os proc. disponibilizados.
begin
  for r in (select * from funcionario where id_tipo_funcionario = 2) loop
    insert into bonus_camareiras(id_bonus_camareira) values(r.id);
  end loop;
end;
/

--Camareiras.
-- Tiago Monteiro - Adicionar o id_bonus_camareira de cada camareira. Neste caso o id da camareira vai ser igual ao do seu bonus. Aproveitei os proc. disponibilizados.
begin
  for r in (select * from funcionario where id_tipo_funcionario = 2) loop
    insert into camareira(id, id_bonus_desta_camareira) values(r.id, r.id);
  end loop;
end;
/

--- END -- 1081699 - Tiago Monteiro - Altera��es para exercisios Aluno B do trabalho 2




--Artigos de consumo
declare
  v_count int := 30;
  v_label varchar(50) := 'Artigo consumo';
  v_preco number;
begin
  for i in 1..v_count loop
    insert into artigo_consumo(id, nome, preco) values(seq_artigo_consumo.nextval, v_label || ' ' || i, mod(v_count, i)+1);
  end loop;
end;
/

--Modos de pagamento
insert into modo_pagamento(id, nome) values(1, 'Numer�rio');
insert into modo_pagamento(id, nome) values(2, 'Cheque');
insert into modo_pagamento(id, nome) values(3, 'Cart�o de cr�dito');
insert into modo_pagamento(id, nome) values(4, 'Cart�o de d�bito');

--Estados reserva
insert into estado_reserva(id, nome) values(1, 'Aberta');
insert into estado_reserva(id, nome) values(2, 'Check-in');
insert into estado_reserva(id, nome) values(3, 'Check-out');
insert into estado_reserva(id, nome) values(4, 'Cancelada');
insert into estado_reserva(id, nome) values(5, 'Finalizada');

declare
  v_count int := 30;
  v_label varchar(50) := 'Artigo consumo';
  v_preco number;
begin
  for i in 1..v_count loop
    insert into artigo_consumo(id, nome, preco) values(seq_artigo_consumo.nextval, v_label || ' ' || i, mod(v_count, i)+1);
  end loop;
end;
/

--�pocas
insert into epoca(id, nome, data_ini, data_fim) values(1, '�poca 1', to_date('2020-01-01', 'yyyy-mm-dd'), to_date('2020-03-31', 'yyyy-mm-dd'));
insert into epoca(id, nome, data_ini, data_fim) values(2, '�poca 2', to_date('2020-04-01', 'yyyy-mm-dd'), to_date('2020-06-30', 'yyyy-mm-dd'));
insert into epoca(id, nome, data_ini, data_fim) values(3, '�poca 3', to_date('2020-07-01', 'yyyy-mm-dd'), to_date('2020-09-30', 'yyyy-mm-dd'));
insert into epoca(id, nome, data_ini, data_fim) values(4, '�poca 4', to_date('2020-09-01', 'yyyy-mm-dd'), to_date('2020-12-31', 'yyyy-mm-dd'));

--Pre�os por �poca e tipo de quarto
declare
  v_preco number;
begin
  for r1 in (select * from epoca) loop
    for r2 in (select * from tipo_quarto) loop
      v_preco := mod(r1.id*r2.id*10, 20)+30;
      insert into preco_epoca_tipo_quarto(id_epoca, id_tipo_quarto, preco) values(r1.id, r2.id, v_preco);
    end loop;
  end loop;
end;
/

insert into tipo_intervencao(id, nome) values(1, 'Limpeza');
insert into tipo_intervencao(id, nome) values(2, 'Manuten��o');

--reservas (N por cada dia, de 1-1-2020 at� 31-12-2020)
declare
  k_count int := 10;
  v_count int;
  v_label varchar(50) := 'Reserva';
  v_di date;
  v_df date;
  v_d date;
  v_id_cliente cliente.id%type;
  v_step int := 7; --de K em K reservas escolho efetivamente um cliente.
  v_id_reserva int;
  v_nr_pessoas int;
  v_id_tipo_quarto int;
  v_dias int;
  v_lag_dias int;
  v_preco number;
begin
  delete from reserva;
  v_di := to_date('2020-01-01', 'yyyy-mm-dd');
  v_df := to_date('2020-12-31', 'yyyy-mm-dd');
  v_d := v_di;
  while v_d < v_df loop
    for i in 1..k_count loop
      v_id_reserva := seq_reserva.nextval;
      --gerar alguns clientes
      select count(0) into v_count from cliente;
      v_id_cliente := mod(v_count, v_id_reserva-(trunc(v_id_reserva/v_count))*v_count);
      if v_id_cliente <= 10 then v_id_cliente := null; end if;
      --tipo de quarto � obtido ciclicamente
      select count(0) into v_count from tipo_quarto;
      v_id_tipo_quarto := mod(v_id_reserva, v_count)+1;
      --N� de dias da reserva � obtido ciclicamente.
      v_dias := mod(v_id_reserva, 3)+1;
      v_lag_dias := mod(v_id_reserva, 30);
      --N� de pessoas(1 at� 4)
      v_nr_pessoas := mod(v_id_reserva, 4) + 1;
      --Pre�o
      begin
        select a.preco into v_preco
          from preco_epoca_tipo_quarto a join epoca b on (a.id_epoca = b.id)
         where id_tipo_quarto = v_id_tipo_quarto
           and v_d+v_lag_dias between b.data_ini and b.data_fim; 
      exception
        when others then
          v_preco := 35;
      end;
      --
      insert into reserva(id, id_cliente, nome, id_tipo_quarto, data, data_entrada, data_saida, nr_pessoas, preco, id_estado_reserva)
      values(v_id_reserva, v_id_cliente, 'Cliente reserva '||v_id_reserva, v_id_tipo_quarto, v_d, v_d+v_lag_dias, v_d+v_lag_dias+v_dias, v_nr_pessoas, v_preco*v_dias, 1);
    end loop;
    ----
    v_d := v_d + 1;
  end loop;
end;
/

--
commit;

-- INSERTS RESERVA (PONTO 1 & 2)-- Aluno A - 1181487 - Carlos Valente

-- Insert para testar o estado da reserva
insert into reserva(id, id_cliente, nome, id_tipo_quarto, data, data_entrada, data_saida, nr_pessoas, preco, id_estado_reserva)
values(3651, 192, 'Cliente reserva '||3651, 3, to_date('2020-01-31', 'yyyy-mm-dd'), to_date('2020-02-08', 'yyyy-mm-dd'), to_date('2020-02-11', 'yyyy-mm-dd'), 1, 120, 2);

-- 
insert into reserva(id, id_cliente, nome, id_tipo_quarto, data, data_entrada, data_saida, nr_pessoas, preco, id_estado_reserva)
values(3652, 192, 'Cliente reserva '||3652, 3, to_date('2020-01-31', 'yyyy-mm-dd'), to_date('2020-02-08', 'yyyy-mm-dd'), to_date('2020-02-11', 'yyyy-mm-dd'), 1, 120, 1);

insert into checkin(id_reserva, data)
values (3652, to_date('2020-02-08', 'yyyy-mm-dd'));


-- INSERTS CONTA_CONSUMO (PONTO 4)-- Aluno B - 1081699 - Tiago Monteiro

insert into conta_consumo(id, data_abertura, id_reserva) values(1, to_date('2020-01-01', 'yyyy-mm-dd'),106);
insert into conta_consumo(id, data_abertura, id_reserva) values(2, to_date('2020-02-01', 'yyyy-mm-dd'),107);
insert into conta_consumo(id, data_abertura, id_reserva) values(3, to_date('2020-03-01', 'yyyy-mm-dd'),108);
insert into conta_consumo(id, data_abertura, id_reserva) values(4, to_date('2020-04-01', 'yyyy-mm-dd'),109);
insert into conta_consumo(id, data_abertura, id_reserva) values(5, to_date('2020-05-01', 'yyyy-mm-dd'),110);
insert into conta_consumo(id, data_abertura, id_reserva) values(6, to_date('2020-06-01', 'yyyy-mm-dd'),111);
insert into conta_consumo(id, data_abertura, id_reserva) values(7, to_date('2020-07-01', 'yyyy-mm-dd'),112);
insert into conta_consumo(id, data_abertura, id_reserva) values(8, to_date('2020-08-01', 'yyyy-mm-dd'),113);
insert into conta_consumo(id, data_abertura, id_reserva) values(9, to_date('2020-09-01', 'yyyy-mm-dd'),114);
insert into conta_consumo(id, data_abertura, id_reserva) values(10, to_date('2020-10-01', 'yyyy-mm-dd'),115);
insert into conta_consumo(id, data_abertura, id_reserva) values(11, to_date('2020-11-01', 'yyyy-mm-dd'),116);
insert into conta_consumo(id, data_abertura, id_reserva) values(12, to_date('2020-12-01', 'yyyy-mm-dd'),117);
insert into conta_consumo(id, data_abertura, id_reserva) values(13, to_date('2019-01-01', 'yyyy-mm-dd'),118);
insert into conta_consumo(id, data_abertura, id_reserva) values(14, to_date('2019-02-01', 'yyyy-mm-dd'),119);
insert into conta_consumo(id, data_abertura, id_reserva) values(15, to_date('2019-03-01', 'yyyy-mm-dd'),120);
insert into conta_consumo(id, data_abertura, id_reserva) values(16, to_date('2019-04-01', 'yyyy-mm-dd'),121);
insert into conta_consumo(id, data_abertura, id_reserva) values(17, to_date('2019-05-01', 'yyyy-mm-dd'),122);
insert into conta_consumo(id, data_abertura, id_reserva) values(18, to_date('2019-06-01', 'yyyy-mm-dd'),123);
insert into conta_consumo(id, data_abertura, id_reserva) values(19, to_date('2019-07-01', 'yyyy-mm-dd'),124);
insert into conta_consumo(id, data_abertura, id_reserva) values(20, to_date('2019-08-01', 'yyyy-mm-dd'),125);
insert into conta_consumo(id, data_abertura, id_reserva) values(21, to_date('2019-09-01', 'yyyy-mm-dd'),126);
insert into conta_consumo(id, data_abertura, id_reserva) values(22, to_date('2019-10-01', 'yyyy-mm-dd'),127);
insert into conta_consumo(id, data_abertura, id_reserva) values(23, to_date('2019-11-01', 'yyyy-mm-dd'),128);
insert into conta_consumo(id, data_abertura, id_reserva) values(24, to_date('2019-12-01', 'yyyy-mm-dd'),129);



-- INSERTS LINHA_CONTA_CONSUMO (PONTO 4)-- Aluno B - 1081699 - Tiago Monteiro

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(1,1,1,to_date('2020-01-01', 'yyyy-mm-dd'),1,1,11);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(1,2,1,to_date('2020-01-02', 'yyyy-mm-dd'),1,1,12);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(1,3,1,to_date('2020-01-03', 'yyyy-mm-dd'),1,1,13);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(1,4,1,to_date('2020-01-04', 'yyyy-mm-dd'),1,1,11);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(2,1,2,to_date('2020-02-01', 'yyyy-mm-dd'),120,1,15); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(2,2,2,to_date('2020-02-02', 'yyyy-mm-dd'),121,1,17); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(2,3,2,to_date('2020-02-03', 'yyyy-mm-dd'),122,1,19); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(2,4,2,to_date('2020-02-04', 'yyyy-mm-dd'),123,1,20); -- quantidades grandes para ponto 5

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(3,1,3,to_date('2020-03-01', 'yyyy-mm-dd'),3,1,16);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(3,2,3,to_date('2020-03-02', 'yyyy-mm-dd'),2,1,17);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(3,3,3,to_date('2020-03-03', 'yyyy-mm-dd'),1,1,18);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(3,4,3,to_date('2020-03-04', 'yyyy-mm-dd'),1,1,19);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(4,1,4,to_date('2020-04-01', 'yyyy-mm-dd'),1,3,15);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(4,2,4,to_date('2020-04-02', 'yyyy-mm-dd'),2,3,11);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(4,3,4,to_date('2020-04-03', 'yyyy-mm-dd'),2,3,12);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(4,4,4,to_date('2020-04-04', 'yyyy-mm-dd'),1,3,17);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(5,1,5,to_date('2020-05-01', 'yyyy-mm-dd'),2,1,15);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(5,2,5,to_date('2020-05-02', 'yyyy-mm-dd'),2,1,14);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(5,3,5,to_date('2020-05-03', 'yyyy-mm-dd'),2,1,13);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(5,4,5,to_date('2020-05-04', 'yyyy-mm-dd'),2,1,12);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(6,1,6,to_date('2020-06-01', 'yyyy-mm-dd'),1,1,14);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(6,2,6,to_date('2020-06-02', 'yyyy-mm-dd'),3,1,16);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(6,3,6,to_date('2020-06-03', 'yyyy-mm-dd'),2,1,18);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(6,4,6,to_date('2020-06-04', 'yyyy-mm-dd'),1,1,20);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(7,1,7,to_date('2020-07-01', 'yyyy-mm-dd'),2,3,15);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(7,2,7,to_date('2020-07-02', 'yyyy-mm-dd'),1,3,13);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(7,3,7,to_date('2020-07-03', 'yyyy-mm-dd'),1,3,12);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(7,4,7,to_date('2020-07-04', 'yyyy-mm-dd'),1,3,17);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(8,1,8,to_date('2020-08-01', 'yyyy-mm-dd'),2,7,14);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(8,2,8,to_date('2020-08-02', 'yyyy-mm-dd'),2,7,13);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(8,3,8,to_date('2020-08-03', 'yyyy-mm-dd'),1,7,12);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(8,4,8,to_date('2020-08-04', 'yyyy-mm-dd'),2,7,11);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(9,1,9,to_date('2020-09-01', 'yyyy-mm-dd'),4,4,11);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(9,2,9,to_date('2020-09-02', 'yyyy-mm-dd'),3,4,15);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(9,3,9,to_date('2020-09-03', 'yyyy-mm-dd'),2,4,19);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(9,4,9,to_date('2020-09-04', 'yyyy-mm-dd'),1,4,20);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(10,1,10,to_date('2020-10-01', 'yyyy-mm-dd'),1,1,18);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(10,2,10,to_date('2020-10-02', 'yyyy-mm-dd'),1,1,16);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(10,3,10,to_date('2020-10-03', 'yyyy-mm-dd'),1,1,14);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(10,4,10,to_date('2020-10-04', 'yyyy-mm-dd'),1,1,12);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(11,1,11,to_date('2020-11-01', 'yyyy-mm-dd'),2,9,17);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(11,2,11,to_date('2020-11-02', 'yyyy-mm-dd'),2,9,15);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(11,3,11,to_date('2020-11-03', 'yyyy-mm-dd'),2,9,13);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(11,4,11,to_date('2020-11-04', 'yyyy-mm-dd'),2,9,11);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(12,1,12,to_date('2020-12-01', 'yyyy-mm-dd'),1,7,15);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(12,2,12,to_date('2020-12-02', 'yyyy-mm-dd'),2,7,16);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(12,3,12,to_date('2020-12-03', 'yyyy-mm-dd'),3,7,15);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(12,4,12,to_date('2020-12-04', 'yyyy-mm-dd'),4,7,16);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(13,1,13,to_date('2019-01-01', 'yyyy-mm-dd'),1,5,11);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(13,2,13,to_date('2019-01-02', 'yyyy-mm-dd'),3,5,12);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(13,3,13,to_date('2019-01-03', 'yyyy-mm-dd'),4,5,13);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(13,4,13,to_date('2019-01-04', 'yyyy-mm-dd'),1,5,14);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(14,1,14,to_date('2019-01-01', 'yyyy-mm-dd'),2,3,18);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(14,2,14,to_date('2019-02-02', 'yyyy-mm-dd'),2,3,19);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(14,3,14,to_date('2019-02-03', 'yyyy-mm-dd'),1,3,20);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(14,4,14,to_date('2019-02-04', 'yyyy-mm-dd'),3,3,20);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(15,1,15,to_date('2019-03-01', 'yyyy-mm-dd'),1,1,12);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(15,2,15,to_date('2019-03-02', 'yyyy-mm-dd'),1,1,16);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(15,3,15,to_date('2019-03-03', 'yyyy-mm-dd'),1,1,15);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(15,4,15,to_date('2019-03-05', 'yyyy-mm-dd'),1,1,11);

---

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(16,1,16,to_date('2019-04-01', 'yyyy-mm-dd'),10,15,14); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(16,2,16,to_date('2019-04-02', 'yyyy-mm-dd'),9,15,15); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(16,3,16,to_date('2019-04-03', 'yyyy-mm-dd'),8,15,16); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(16,4,16,to_date('2019-04-04', 'yyyy-mm-dd'),7,15,17); -- quantidades grandes para ponto 5

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(17,1,17,to_date('2019-05-01', 'yyyy-mm-dd'),10,14,13); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(17,2,17,to_date('2019-05-02', 'yyyy-mm-dd'),10,14,20); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(17,3,17,to_date('2019-05-03', 'yyyy-mm-dd'),10,14,19); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(17,4,17,to_date('2019-05-04', 'yyyy-mm-dd'),10,14,18); -- quantidades grandes para ponto 5

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(18,1,18,to_date('2019-06-01', 'yyyy-mm-dd'),50,13,19); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(18,2,18,to_date('2019-06-02', 'yyyy-mm-dd'),50,13,20); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(18,3,18,to_date('2019-06-03', 'yyyy-mm-dd'),50,13,11); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(18,4,18,to_date('2019-06-04', 'yyyy-mm-dd'),50,13,12); -- quantidades grandes para ponto 5

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(19,1,19,to_date('2019-07-01', 'yyyy-mm-dd'),100,12,15); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(19,2,19,to_date('2019-07-02', 'yyyy-mm-dd'),100,12,16); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(19,3,19,to_date('2019-07-03', 'yyyy-mm-dd'),100,12,17); -- quantidades grandes para ponto 5
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(19,4,19,to_date('2019-07-04', 'yyyy-mm-dd'),100,12,18); -- quantidades grandes para ponto 5

---

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(20,1,20,to_date('2019-08-01', 'yyyy-mm-dd'),1,11,11);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(20,2,20,to_date('2019-08-02', 'yyyy-mm-dd'),3,11,12);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(20,3,20,to_date('2019-08-03', 'yyyy-mm-dd'),3,11,13);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(20,4,20,to_date('2019-08-04', 'yyyy-mm-dd'),3,11,14);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(21,1,21,to_date('2019-09-01', 'yyyy-mm-dd'),3,10,11);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(21,2,21,to_date('2019-09-02', 'yyyy-mm-dd'),3,10,20);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(21,3,21,to_date('2019-09-03', 'yyyy-mm-dd'),3,10,19);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(21,4,21,to_date('2019-09-04', 'yyyy-mm-dd'),2,10,19);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(22,1,22,to_date('2019-10-01', 'yyyy-mm-dd'),1,9,18);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(22,2,22,to_date('2019-10-02', 'yyyy-mm-dd'),2,9,17);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(22,3,22,to_date('2019-10-03', 'yyyy-mm-dd'),3,9,16);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(22,4,22,to_date('2019-10-04', 'yyyy-mm-dd'),4,9,15);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(23,1,23,to_date('2019-11-01', 'yyyy-mm-dd'),2,8,14);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(23,2,23,to_date('2019-11-02', 'yyyy-mm-dd'),2,8,12);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(23,3,23,to_date('2019-11-03', 'yyyy-mm-dd'),2,8,13);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(23,4,23,to_date('2019-11-04', 'yyyy-mm-dd'),2,8,11);

insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(24,1,24,to_date('2019-12-01', 'yyyy-mm-dd'),1,7,17);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(24,2,24,to_date('2019-12-02', 'yyyy-mm-dd'),2,7,13);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(24,3,24,to_date('2019-12-03', 'yyyy-mm-dd'),3,7,20);
insert into linha_conta_consumo(id_conta_consumo, linha, id_artigo_consumo, data_registo, quantidade, preco_unitario, id_camareira) values(24,4,24,to_date('2019-12-04', 'yyyy-mm-dd'),1,7,20);