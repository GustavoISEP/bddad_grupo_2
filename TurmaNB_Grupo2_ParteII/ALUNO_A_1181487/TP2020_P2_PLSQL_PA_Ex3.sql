/*Aluno 1181487 do Grupo NB2*/

/*Implementar um trigger designado trgEpocasNaoSobrepostas que garanta que a
inserção/alteração de uma época não conduz a sobreposição entre épocas. Testar
adequadamente o trigger implementado.*/

create or replace TRIGGER trgEpocasNaoSobrepostas
BEFORE INSERT OR UPDATE ON EPOCA
FOR EACH ROW

DECLARE
data_inicio INTEGER;
data_fim INTEGER;
EX_ERRO EXCEPTION;
EX_ERRO_DATA_INI EXCEPTION;
EX_ERRO_DATA_FIM EXCEPTION;

BEGIN
	
	--verifica se a data de inicio a ser inserida está compreendida em alguma epoca existente
	SELECT COUNT(*) INTO data_inicio 
	FROM EPOCA E
	WHERE :NEW.DATA_INI BETWEEN E.DATA_INI AND E.DATA_FIM;
	
	--verifica se a data de fim a ser inserida está compreendida em alguma epoca existente
	SELECT COUNT(*) INTO data_fim 
	FROM EPOCA E
	WHERE :NEW.DATA_FIM BETWEEN E.DATA_INI AND E.DATA_FIM;

    IF data_inicio > 0 THEN
		RAISE EX_ERRO_DATA_INI; --se data de inicio já existe num intervalo existente lança exceção
    ELSIF data_fim > 0 THEN
        RAISE EX_ERRO_DATA_FIM; --se data de fim já existe num intervalo existente lança exceção
    
	END IF;

	EXCEPTION
    WHEN EX_ERRO_DATA_INI THEN
		RAISE_APPLICATION_ERROR(-20001, 'Erro: Data de ínicio dentro de época existente.');
    WHEN EX_ERRO_DATA_FIM THEN
		RAISE_APPLICATION_ERROR(-20002, 'Erro: Data de fim dentro de época existente.');

END trgEpocasNaoSobrepostas;
/

-- Consultar tabela onde o trigger está ativo
select * from EPOCA;

-- Neste teste deve retornar a mensagem "Erro: Data de ínicio dentro de época existente.".
insert into epoca(id, nome, data_ini, data_fim) values(5, 'Época 5', to_date('2020-09-15', 'yyyy-mm-dd'), to_date('2021-01-15', 'yyyy-mm-dd'));

-- Neste teste deve retornar a mensagem "Erro: Data de fim dentro de época existente.".
insert into epoca(id, nome, data_ini, data_fim) values(5, 'Época 5', to_date('2019-10-01', 'yyyy-mm-dd'), to_date('2020-02-01', 'yyyy-mm-dd'));

-- Neste teste deve retornar a mensagem "1 row inserted.".
insert into epoca(id, nome, data_ini, data_fim) values(5, 'Época 5', to_date('2021-01-01', 'yyyy-mm-dd'), to_date('2021-04-01', 'yyyy-mm-dd'));