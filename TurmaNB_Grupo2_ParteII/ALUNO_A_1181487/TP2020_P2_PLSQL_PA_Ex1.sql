/*Aluno 1181487 do Grupo NB2*/

/*Implementar uma função designada fncGetQuartoReserva que, dado o ID de uma
reserva, retorne o quarto a alocar. Havendo mais que um quarto, pretende-se o que
estiver no andar mais baixo e, depois, que tenha tido menos dias de ocupação no último
ano (para promover a distribuição da utilização pelos diversos quartos). No caso de o
parâmetro ser inválido ou não haver quarto disponível, a função deve retornar o valor
NULL através do mecanismo de exceções. O parâmetro é inválido quando: a) é null, b)
não existe, c) a reserva já tem um quarto associado, d) o estado da reserva não é o
adequado (e.g. está fechada). A função tem que tirar o máximo proveito do código SQL.
Testar adequadamente a função implementada, através de blocos anónimos. */

create or replace function 
fncGetQuartoReserva(param_id RESERVA.ID%TYPE)	
return QUARTO.ID%TYPE 
is
quartoAlocar QUARTO.ID%TYPE;
tipoQuarto RESERVA.ID_TIPO_QUARTO%TYPE;
nPessoas RESERVA.NR_PESSOAS%TYPE;
dataIn RESERVA.DATA_ENTRADA%TYPE;
dataOut RESERVA.DATA_SAIDA%TYPE;

c_reserva INTEGER;
quartoAtribuido INTEGER;
estadoReserva ESTADO_RESERVA.ID%TYPE;

param_id_error EXCEPTION;
id_reserva_error EXCEPTION;
quarto_atribuido_erro EXCEPTION;
estado_reserva_error EXCEPTION;

begin
    IF param_id IS NULL THEN
    RAISE param_id_error; --se param_id nulo lança exceção
    END IF;
        
	--verifica se reserva existe
	SELECT COUNT(*) INTO c_reserva
    FROM RESERVA
	WHERE RESERVA.ID = param_id;
	dbms_output.put_line('c_reserva: ');
	dbms_output.put_line(c_reserva);
	--verifica se quarto já se encontra atribuído
	SELECT COUNT(CK.ID_QUARTO) INTO quartoAtribuido
	FROM RESERVA RS
	LEFT JOIN CHECKIN CK ON RS.ID = CK.ID_RESERVA
    WHERE RS.ID = param_id;
	dbms_output.put_line('quartoAtribuido: ');
	dbms_output.put_line(quartoAtribuido);
	--verifica estado da reserva
	SELECT ID_ESTADO_RESERVA INTO estadoReserva
	FROM RESERVA RS
    WHERE RS.ID = param_id;
	dbms_output.put_line('estadoReserva: ');
	dbms_output.put_line(estadoReserva);
	
	IF c_reserva < 1 OR c_reserva IS NULL THEN
		RAISE id_reserva_error; --se reserva não existe lança exceção
	ELSIF quartoAtribuido > 0 THEN
		RAISE quarto_atribuido_erro; --se quarto se encontra atribuido lança exceção
	ELSIF estadoReserva > 1 THEN
		RAISE estado_reserva_error; --se estado da reserva diferente de aberta
	ELSE
		--seleciona qual o tipo de quarto da reserva
		SELECT ID_TIPO_QUARTO into tipoQuarto
		FROM RESERVA
		WHERE ID = param_id;
		
		--seleciona o número de pessoas da reserva
		SELECT NR_PESSOAS into nPessoas
		FROM RESERVA
		WHERE ID = param_id;
		
		--seleciona a data de entrada 
		SELECT DATA_ENTRADA into dataIn
		FROM RESERVA
		WHERE ID = param_id;
		
		--seleciona a data de saída
		SELECT DATA_SAIDA into dataOut
		FROM RESERVA
		WHERE ID = param_id;
		
		--seleciona o id_quarto disponivel, seguindo os requisitos do enunciado
		SELECT ID_QUARTO INTO quartoAlocar FROM(SELECT CKI.ID_QUARTO, QRT.ID_TIPO_QUARTO, COUNT(CKI.ID_QUARTO) AS UTILIZACOES, ADR.NR_ANDAR
		FROM CHECKIN CKI
		INNER JOIN QUARTO QRT ON CKI.ID_QUARTO = QRT.ID
		INNER JOIN ANDAR ADR ON QRT.ID_ANDAR = ADR.ID
		WHERE CKI.ID_QUARTO = QRT.ID 
			AND QRT.ID_TIPO_QUARTO = tipoQuarto --verifica o tipo de quarto
			AND QRT.LOTACAO_MAXIMA >= nPessoas --verifica a capacidade do quarto
			AND CKI.DATA BETWEEN ADD_MONTHS(dataIn,-12) AND dataIn --verifica os quartos do último ano
			AND CKI.ID_QUARTO NOT IN (SELECT ID_QUARTO 
										FROM CHECKIN CKIQ
										INNER JOIN CHECKOUT CKOQ ON CKIQ.ID_RESERVA = CKOQ.ID_RESERVA
										WHERE CKIQ.DATA < dataIn AND CKOQ.DATA > dataIn AND CKOQ.DATA > dataOut) --verifica os quartos pela data de entrada e de saída
			AND rownum=1 --permite ir apenas buscar o primeiro registo
		GROUP BY CKI.ID_QUARTO, QRT.ID_TIPO_QUARTO, ADR.NR_ANDAR
		ORDER BY ADR.NR_ANDAR, UTILIZACOES ASC);

		RETURN quartoAlocar;
    END IF;
	
	EXCEPTION
    WHEN param_id_error THEN
		return null;
        --raise_application_error(-20000, 'Parâmetro nulo.');
	WHEN id_reserva_error OR no_data_found THEN
		return null;
        --raise_application_error(-20001, 'A Reserva não existe.');
	WHEN quarto_atribuido_erro THEN
		return null;
        --raise_application_error(-20002, 'Reserva com quarto atribuído.');
	WHEN estado_reserva_error THEN
		return null;
        --raise_application_error(-20003, 'Estado da reserva incorreto.');	
end fncGetQuartoReserva;	
/

-- Neste teste deve retornar a mensagem "Parâmetro nulo.".
DECLARE
bQuartos   QUARTO.ID%type;
bReserva   RESERVA.ID%type;
BEGIN
bReserva:= NULL;
bQuartos := fncGetQuartoReserva(bReserva);
dbms_output.put_line('Quarto de acordo com o pretendido: ');
dbms_output.put_line(bQuartos);
END;

-- Neste teste deve retornar a mensagem "A Reserva não existe.".
DECLARE
bQuartos   QUARTO.ID%type;
bReserva   RESERVA.ID%type;
BEGIN
bReserva:= 5555;
bQuartos := fncGetQuartoReserva(bReserva);
dbms_output.put_line('Quarto de acordo com o pretendido: ');
dbms_output.put_line(bQuartos);
END;

-- Neste teste deve retornar a mensagem "Estado da reserva incorreto.".
DECLARE
bQuartos   QUARTO.ID%type;
bReserva   RESERVA.ID%type;
BEGIN
bReserva:= 308;
bQuartos := fncGetQuartoReserva(bReserva);
dbms_output.put_line('Quarto de acordo com o pretendido: ');
dbms_output.put_line(bQuartos);
END;

-- Neste teste deve retornar a mensagem "Reserva com quarto atribuído.".
DECLARE
bQuartos   QUARTO.ID%type;
bReserva   RESERVA.ID%type;
BEGIN
bReserva:= 3651;
bQuartos := fncGetQuartoReserva(bReserva);
dbms_output.put_line('Quarto de acordo com o pretendido: ');
dbms_output.put_line(bQuartos);
END;