/*Aluno 1181487 do Grupo NB2*/

/*Implementar um procedimento designado prcCheckOut para efetuar o check-out de um
quarto, incluindo a geração da fatura. O procedimento deve receber por parâmetro a
linha da tabela reserva correspondente à reserva do quarto. Se o parâmetro fornecido for
inválido, o procedimento deve levantar uma exceção com uma mensagem apropriada. O
procedimento tem que tirar o máximo proveito do código SQL. Testar adequadamente o
procedimento implementado, através de blocos anónimos.*/

create or replace PROCEDURE prcCheckOut(
param_id RESERVA.ID%TYPE) IS

id_reserva_inexistente EXCEPTION;
id_checkin_inexistente EXCEPTION;
p_id_reserva_int INTEGER;
p_id_reserva RESERVA.ID%type;
p_id_reserva_chkout RESERVA.DATA_SAIDA%type;
p_id_reserva_chkin CHECKIN.ID_RESERVA%type;
p_id_fatura FATURA.ID%type;
c_fatura INTEGER;

BEGIN
	--seleciona o id reserva pelo parâmetro
    SELECT COUNT(ID) INTO p_id_reserva_int
	FROM RESERVA
	WHERE ID = param_id;
    
    SELECT ID INTO p_id_reserva
	FROM RESERVA
	WHERE ID = param_id;
	
	--seleciona o id reserva do checkin pelo parâmetro
	SELECT ID_RESERVA INTO p_id_reserva_chkin
	FROM CHECKIN
	WHERE ID_RESERVA = param_id;

	--seleciona a data de saida da reserva pelo parâmetro
	SELECT DATA_SAIDA INTO p_id_reserva_chkout
    FROM RESERVA
    WHERE ID = param_id;
    
	--verifica se já existem faturas
    SELECT COUNT(*) INTO c_fatura
    FROM FATURA;
    
    IF c_fatura = 0 THEN p_id_fatura:= 0; --se nºao existe fatura coloca o id a 0
    ELSE
		--seleciona o id da última fatura registada 
        SELECT ID INTO p_id_fatura
        FROM FATURA
        WHERE rownum=1
        ORDER BY ID DESC;
    END IF;

    IF p_id_reserva_int < 1 OR p_id_reserva_int IS NULL THEN
		RAISE id_reserva_inexistente; --se reserva não existe lança exceção
	ELSIF p_id_reserva_chkin = NULL THEN
		RAISE id_checkin_inexistente; --se reserva não tem check-in lança exceção
	ELSE
        DBMS_OUTPUT.PUT_LINE(p_id_reserva); 
        DBMS_OUTPUT.PUT_LINE(p_id_reserva_chkout); 
        DBMS_OUTPUT.PUT_LINE(p_id_fatura+1); 
        insert into checkout(id_reserva, data) values (p_id_reserva, to_date(p_id_reserva_chkout, 'yyyy-mm-dd'));
        insert into fatura(ID, NUMERO ,DATA, ID_RESERVA) values (p_id_fatura+1, p_id_fatura+1, p_id_reserva_chkout, p_id_reserva);
    END IF;

	EXCEPTION
    WHEN id_reserva_inexistente OR id_checkin_inexistente OR no_data_found THEN
        raise_application_error(-20000, 'A Reserva não existe, ou o checkin não existe, não pode fazer check-out.');
	--WHEN id_checkin_inexistente THEN
        --raise_application_error(-20001, 'A Reserva ainda não tem check-in, não pode fazer check-out'); 
END prcCheckOut;
/

-- Teste para despoltar erro de reserva/checkin inexistente.
begin
  prcCheckOut(5000);
end;
/

-- Teste funcional para inserir chekout e fatura
begin
  prcCheckOut(3652);
end;
/