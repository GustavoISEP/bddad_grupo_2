set serveroutput on;

create or replace function fncObterRegistoMensalCamareira(mes in number, ano_param in number default 0) return sys_refcursor
is
  v_rc sys_refcursor;
  ano number; --ano a usar no seclet
  dias_mes DATE;
  
begin
if ano_param = 0 then ano := extract(year from sysdate - 365); else ano := ano_param; end if; --se não tiver sido colocado o ano_param (fica a 0), o ano é o  anterior ao atual.
dias_mes := TO_DATE(ano||'-'||mes||'-01', 'YYYY-MM-DD');
--dbms_output.put_line ('[Validacao/Teste Unitario] Ano.Mês.Dia: ' ||dias_mes);
--dbms_output.put_line ('');
OPEN v_rc FOR

--Considerei que as camareiras sem qualquer registo de consumos neste mes/ano não são apresentadas no resultado.    
SELECT linha_conta_consumo.id_camareira as "ID Camareira",
funcionario.nome as "Nome Camareira",
sum(linha_conta_consumo.preco_unitario * linha_conta_consumo.quantidade) as Total_Consumos_Registados,
min (linha_conta_consumo.data_registo) as "Data Primeiro Registo", --Data primeiro registo da camareira no mes selecionado
max (linha_conta_consumo.data_registo) as "Data Ultimo Registo", --Data ultimo registo da camareira no mes selecionado
EXTRACT( DAY FROM TO_DATE(LAST_DAY(dias_mes))) - count (DISTINCT data_registo) as "Qtd dias sem registos" --obter numero de dias do mês selecionado e substrair aos dias unicos com registos
from linha_conta_consumo
INNER JOIN funcionario 
ON linha_conta_consumo.id_camareira = funcionario.id
WHERE extract(month from linha_conta_consumo.data_registo) = mes 
AND extract(year from linha_conta_consumo.data_registo) = ano
group by linha_conta_consumo.id_camareira, funcionario.nome --agrupar por o que "quero"#
order by Total_Consumos_Registados DESC;
	
RETURN  (v_rc);
end;
/


--- 1081699 - Tiago Monteiro
--- Testar a função

DECLARE
 resultado sys_refcursor;
 id_da_camareira  camareira.id%type;
 nome_da_camareira  funcionario.nome%type;
 total_consumos linha_conta_consumo.preco_unitario%type;
 data_primeiro_registo  linha_conta_consumo.data_registo%type;
 data_ultimo_registo  linha_conta_consumo.data_registo%type;
 numero_dias_sem_registos number;
 
BEGIN
    dbms_output.put_line ('ID da camareira' || ' | ' || 'Nome da camareira' || ' | ' || 'Valor total consumos registados' || ' | ' || 'Data primeiro registo'  || ' | ' || 'Data ultimo registo' || ' | ' || 'Numero de dias sem registos');
    resultado:=fncObterRegistoMensalCamareira (04,2019);
    LOOP
        Fetch resultado into id_da_camareira, nome_da_camareira, total_consumos, data_primeiro_registo, data_ultimo_registo, numero_dias_sem_registos;
        EXIT WHEN resultado%NOTFOUND;
        dbms_output.put_line (id_da_camareira || ' | ' || nome_da_camareira || ' | ' || total_consumos || ' | ' || data_primeiro_registo  || ' | ' || data_ultimo_registo || ' | ' || numero_dias_sem_registos);
    END LOOP;
END;