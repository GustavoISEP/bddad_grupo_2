create or replace procedure prcAtualizarBonusCamareiras(mes in number, ano_param in number default extract(year from sysdate))
AS	    
    c_camareiras sys_refcursor;    
	id_da_camareira  camareira.id%type;
	nome_da_camareira  funcionario.nome%type;
	total_consumos linha_conta_consumo.preco_unitario%type;
	data_primeiro_registo  linha_conta_consumo.data_registo%type;
	data_ultimo_registo  linha_conta_consumo.data_registo%type;
	numero_dias_sem_registos number;
	v_check number;
    
	
begin 
    
    c_camareiras := fncObterRegistoMensalCamareira (mes,ano_param);
	
	--colocar todos os bonus das camareiras a zero, para limpar dados de calculos anteriores. No apenas refere 
	--"O valor do bónus deve ser registado na tabela de camareiras e será determinado de acordo com o descrito na tabela 1."
	-- por isso não foi implementada qualquer estratégia / tabela para guardar o historico dos bonus.
	--update camareira set bonus_camareira = 0;
	
	LOOP
	fetch c_camareiras into id_da_camareira, nome_da_camareira, total_consumos, data_primeiro_registo, data_ultimo_registo, numero_dias_sem_registos;
    EXIT WHEN c_camareiras%NOTFOUND;
		
		select count(*) into v_check -- validar se já há um registo para esta camareira / ano / mes para ver se é para criar um novo ou atualizar.
		from linhas_bonus_camareiras 
		where id_bonus_camareira = id_da_camareira 
			AND ano_bonus = ano_param 
			AND mes_bonus = mes;
		
        --Debug
        --dbms_output.put_line ('Update(>1) ou Insert(0): ' ||v_check);
        --dbms_output.put_line (id_da_camareira || ' | ' || total_consumos || ' | ' || ano_param || ' | ' || mes );        
        
        --O BETWEEN do oracle é inclusive
        
		if v_check > 0 then -- ja existe um registo e será para atualizar		
            --debug
            --dbms_output.put_line ('Isto não deveria aparecer'); 
			if total_consumos > 1000 then
				update linhas_bonus_camareiras
				set bonus_do_mes = total_consumos*0.15
				where id_bonus_camareira = id_da_camareira 
					and ano_bonus = ano_param
					and mes_bonus = mes;
			end if;
		
            if total_consumos BETWEEN 500 and 1000 then
				update linhas_bonus_camareiras
				set bonus_do_mes = total_consumos*0.10
				where id_bonus_camareira = id_da_camareira 
					and ano_bonus = ano_param
					and mes_bonus = mes;
			end if;
		
			if total_consumos BETWEEN 101 and 499 then
				update linhas_bonus_camareiras
				set bonus_do_mes = total_consumos*0.05
				where id_bonus_camareira = id_da_camareira 
					and ano_bonus = ano_param
					and mes_bonus = mes;
			end if; 
		
			if total_consumos <= 100 then
				update linhas_bonus_camareiras
				set bonus_do_mes = total_consumos*0
				where id_bonus_camareira = id_da_camareira 
					and ano_bonus = ano_param
					and mes_bonus = mes;
			end if;
			
		end if;
		
		if v_check = 0 then -- ainda não existe um registo e será para criar
		
			if total_consumos > 1000 then
                --debug
                --dbms_output.put_line ('Não 1'); 
				insert into linhas_bonus_camareiras(id_bonus_camareira, ano_bonus, mes_bonus, bonus_do_mes) values(id_da_camareira, ano_param, mes, total_consumos*0.15);
			end if; 
		
            if total_consumos BETWEEN 500 and 1000 then
                --debug
                --dbms_output.put_line ('Não 2'); 
				insert into linhas_bonus_camareiras(id_bonus_camareira, ano_bonus, mes_bonus, bonus_do_mes) values(id_da_camareira, ano_param, mes, total_consumos*0.10);
			end if; 
		
            if total_consumos BETWEEN 101 and 499 then
                --debug
                --dbms_output.put_line ('Nao 3');    
				insert into linhas_bonus_camareiras(id_bonus_camareira, ano_bonus, mes_bonus, bonus_do_mes) values(id_da_camareira, ano_param, mes, total_consumos*0.05);
                
			end if; 
		
			if total_consumos <= 100 then
				insert into linhas_bonus_camareiras(id_bonus_camareira, ano_bonus, mes_bonus, bonus_do_mes) values(id_da_camareira, ano_param, mes, 0);
			end if;	
		
		end if;	
		
		
		
	END LOOP;
end;
/

--- 1081699 - Tiago Monteiro
--- Testar o procedimento

-- Teste prcAtualizarBonusCamareiras

-- Correr processamento bonus para 04.2019
-- Os dados desta mes/ano são para dar bonus de 5%
begin
  prcAtualizarBonusCamareiras(04,2019);
end;
/

-- Correr processamento bonus para 05.2019 e 06.2019
-- Os dados desta mes/ano são para dar bonus de 10%
begin
  prcAtualizarBonusCamareiras(05,2019);
end;
/
--
begin
  prcAtualizarBonusCamareiras(06,2019);
end;
/

-- Correr processamento bonus para 07.2019
-- Os dados desta mes/ano são para dar bonus de 15%
begin
  prcAtualizarBonusCamareiras(07,2019);
end;
/

-- Teste não introduzir ano. Terá que assumir 2020
 begin
  prcAtualizarBonusCamareiras(02);
end;
/

-- tabela com os bonos
select * from linhas_bonus_camareiras;