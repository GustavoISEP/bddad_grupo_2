CREATE OR REPLACE TRIGGER trgCorrigirAlteracaoBonus
BEFORE UPDATE ON linhas_bonus_camareiras
FOR EACH ROW


DECLARE
bonus_nao_pode_ser_menor_que_atual EXCEPTION;
aumento_superior_a_50_perc EXCEPTION;

BEGIN

if (:old.bonus_do_mes > :new.bonus_do_mes) then
	raise bonus_nao_pode_ser_menor_que_atual;
end if;
   --Se encomenda já FACTURADA = 1, evitar gerar nova a factura.
if (:new.bonus_do_mes > (:old.bonus_do_mes + (:old.bonus_do_mes/2))) then
	raise aumento_superior_a_50_perc;
end if;


EXCEPTION
  when bonus_nao_pode_ser_menor_que_atual then
    raise_application_error(-20001,'O novo valor de bonus não pode ser inferior ao já existente!');
  when aumento_superior_a_50_perc then
    raise_application_error(-20002,'O aumento do bonus não pode ser superior a 50% do valor atual!');	
    
END trgCorrigirAlteracaoBonus;


-- 1081699 - Tiago Monteiro
-- Testar o trigger

-- consultar tabela onde o trigger está ativo
select * from linhas_bonus_camareiras;

--Testar com dados após correr o proc com os seguintes parametros:
--prcAtualizarBonusCamareiras(04,2019);

-- 11.26 é mais que 50% do atual (7.5). Vai falhar o update
update linhas_bonus_camareiras
set bonus_do_mes = 11.26
where id_bonus_camareira = 14 and ano_bonus = 2019 and mes_bonus = 4;

-- 2 é inferior ao valor atual 7.5. Vai falhar o update
update linhas_bonus_camareiras
set bonus_do_mes = 2
where id_bonus_camareira = 14 and ano_bonus = 2019 and mes_bonus = 4;

-- 11.25 é exatamente um aumento de 50% do atual 7.5. Vai fazer update
update linhas_bonus_camareiras
set bonus_do_mes = 11.25
where id_bonus_camareira = 14 and ano_bonus = 2019 and mes_bonus = 4;

-- 13.5 é um aumento de 20% em relação ao bonus atual (11.25) Vai fazer update para 13.5
update linhas_bonus_camareiras
set bonus_do_mes = 13.5
where id_bonus_camareira = 14 and ano_bonus = 2019 and mes_bonus = 4;