UPDATE Andar SET 
  nome = ? 
WHERE
  numero_andar = ?;

UPDATE Cliente SET 
  nome = ?, 
  email = ?, 
  telefone = ? 
WHERE
  nif = ?;

UPDATE Consumo SET 
  quantidade = ?, 
  id_intervencao = ? 
WHERE
  id_conta = ? AND data_consumo = ? AND id_produto = ?;

UPDATE ContaConsumo SET 
  data_abertura = ?, 
  id_reserva = ? 
WHERE
  id_conta = ?;

UPDATE Epoca SET 
  data_inicio = ?, 
  data_fim = ? 
WHERE
  designacao = ?;

UPDATE Estado SET 
  descricao_estado = ? 
WHERE
  id_estado = ?;

UPDATE Fatura SET 
  valor_total = ?, 
  id_fatura_consumo = ?, 
  id_fatura_reserva = ? 
WHERE
  id_fatura = ?;

UPDATE Fatura_MetodoPagamento SET 
  valor_pago = ? 
WHERE
  id_fatura = ? AND id_metodo_pagamento = ?;

UPDATE FaturaConsumo SET 
  id_conta = ?, 
  valor = ? 
WHERE
  id_fatura_consumo = ?;

UPDATE FaturaReserva SET 
  id_reserva = ?, 
  valor = ? 
WHERE
  id_fatura_reserva = ?;

UPDATE Funcionarios SET 
  nome = ?, 
  telefone = ?, 
  email = ? 
WHERE
  nif = ?;

UPDATE IntervencaoLimpeza SET 
  nif_funcionario = ? 
WHERE
  id_intervencao = ?;

UPDATE IntervencaoManutencao SET 
  nif_funcionario = ?, 
  descricao = ?, 
  id_pedido_intervencao = ? 
WHERE
  id_intervencao = ?;

UPDATE Manutencao SET 
  telefone_servico = ?, 
  id_responsavel = ? 
WHERE
  nif_funcionario = ?;

UPDATE MetodoPagamento SET 
  descricao = ? 
WHERE
  id_metodo_pagamento = ?;

UPDATE Morada SET 
  rua = ?, 
  numero = ?, 
  localidade = ? 
WHERE
  nif = ?;

UPDATE PedidoIntervencao SET 
  data_pedido_intervencao = ?, 
  id_reserva = ? 
WHERE
  id_pedido_intervencao = ?;

UPDATE Produto SET 
  descricao_produto = ?, 
  id_tipo_produto = ?, 
  preco = ? 
WHERE
  id_produto = ?;

UPDATE Quarto SET 
  lotacao = ?, 
  id_tipo_quarto = ? 
WHERE
  numero_quarto = ? AND numero_andar = ?;

UPDATE RegistoIntervencao SET 
  data_intervencao = ?, 
  numero_quarto = ?, 
  numero_andar = ? 
WHERE
  id_intervencao = ?;

UPDATE Reserva SET 
  nif = ?, 
  id_tipo_quarto = ?, 
  numero_pessoas = ?, 
  data_inicio = ?, 
  data_fim = ?, 
  numero_quarto = ?, 
  numero_andar = ? 
WHERE
  id_reserva = ?;

UPDATE TipoProduto SET 
  descricao_tipo = ? 
WHERE
  id_tipo_produto = ?;

UPDATE TipoQuarto SET 
  designacao = ? 
WHERE
  id_tipo_quarto = ?;

UPDATE TipoQuarto_Epoca SET 
  preco = ? 
WHERE
  designacao = ? AND id_tipo_quarto = ?;
