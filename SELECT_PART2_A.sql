-- Apresentar o nome, a localidade e o nome do concelho dos clientes que j�
-- estiveram alojados nos quartos j� reservados pelo cliente cujo nome � Jos� Silva,
-- que � do concelho de Vila Real. Considere s� as reservas �finalizadas� do cliente
-- Jos� Silva;

SELECT cliente.nome as "Nome Cliente", localidade.descricao_localidade as "Localidade", concelho.descricao_concelho as "Concelho" -- select do nome cliente, localidade e concelho
FROM cliente
INNER JOIN localidade ON cliente.id_localidade = localidade.id_localidade
INNER JOIN concelho ON localidade.id_concelho = concelho.id_concelho WHERE
nif IN ( -- apenas para os NIFs:
SELECT reserva.nif from reserva where reserva.nif!=554785485 AND id_reserva IN ( -- NIFs das reservas nos quartos do Jos� Silva, excluindo as dele
SELECT reserva.id_reserva FROM reserva WHERE (numero_quarto || '-' || numero_andar) IN ( -- reservas no mesmo quarto que as do Jos� Silva
select numero_quarto || '-' || numero_andar FROM reserva where nif = 554785485 and id_reserva IN( -- reservas do Jos� Silva e com estado finalizado
select estadoreserva.id_reserva FROM estadoreserva where id_estado = 3)))); -- reservas com estado finalizado