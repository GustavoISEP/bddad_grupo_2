--PARTE 1

--A
/*Apresentar todos os pedidos de intervenção em aberto (intervenção ainda não
registada) alocados a funcionários de manutenção que não fizeram nenhuma
intervenção (registo) nas últimas 48 hora;*/

SELECT PI.*
FROM pedidointervencao PI
INNER JOIN intervencaomanutencao IM ON IM.id_pedido_intervencao = PI.id_pedido_intervencao
INNER JOIN registointervencao RI ON RI.id_intervencao = IM.id_intervencao
WHERE RI.data_intervencao IS NULL AND IM.nif_funcionario NOT IN
(SELECT IM.nif_funcionario
FROM intervencaomanutencao IM
INNER JOIN registointervencao RI ON RI.id_intervencao = IM.id_intervencao
WHERE ROUND((24 * ((select sysdate from dual) - CAST(RI.data_intervencao AS DATE))), 0) < 48);

--B 
/*Apresentar a data, a hora e o nome dos clientes que reservaram quartos
somente durante o mês de Abril e Junho deste ano. No caso de algum cliente ter
reservado um quarto do tipo suite, deverá apresentar a localidade desse cliente
numa coluna intitulada “Zona do País�?. O resultado deverá ser apresentado por
ordem alfabética do nome de cliente e por ordem descendente da data e hora da
reserva*/

SELECT C.nome, ER.data, (case when TQ.designacao = 'Suite' then L.descricao_localidade else '' end) "Zona do País" 
FROM Cliente C
INNER JOIN reserva R ON C.nif = R.nif 
INNER JOIN estadoreserva ER ON R.id_reserva = ER.id_reserva
INNER JOIN localidade L ON C.id_localidade = L.id_localidade
INNER JOIN Tipoquarto TQ ON R.id_tipo_quarto = TQ.id_tipo_quarto
WHERE ER.id_estado = 1 AND EXTRACT(YEAR FROM ER.data) = 2020 AND (EXTRACT(MONTH FROM ER.data) = 4 OR EXTRACT(MONTH FROM ER.data) = 6)
ORDER BY C.nome ASC, ER.data DESC;

--PARTE 2

--A
/*Apresentar o nome, a localidade e o nome do concelho dos clientes que já
estiveram alojados nos quartos já reservados pelo cliente cujo nome é José Silva,
que é do concelho de Vila Real. Considere só as reservas “finalizadas�? do cliente
José Silva*/

SELECT cliente.nome as "Nome Cliente", localidade.descricao_localidade as "Localidade", concelho.descricao_concelho as "Concelho" -- select do nome cliente, localidade e concelho
FROM cliente
INNER JOIN localidade ON cliente.id_localidade = localidade.id_localidade
INNER JOIN concelho ON localidade.id_concelho = concelho.id_concelho WHERE
nif IN ( -- apenas para os NIFs:
SELECT reserva.nif from reserva where reserva.nif!=554785485 AND id_reserva IN ( -- NIFs das reservas nos quartos do José Silva, excluindo as dele
SELECT reserva.id_reserva FROM reserva WHERE (numero_quarto || '-' || numero_andar) IN ( -- reservas no mesmo quarto que as do José Silva
select numero_quarto || '-' || numero_andar FROM reserva where nif = 554785485 and id_reserva IN( -- reservas do José Silva e com estado finalizado
select estadoreserva.id_reserva FROM estadoreserva where id_estado = 3)))); -- reservas com estado finalizado

--B
/*Apresentar por cada mês, apenas para os últimos 6 meses anteriores à data
atual, qual a camareira que mais intervenções fez em quartos cuja duração de
estadia foi superior à média das estadias agrupadas por tipo de quarto*/

--Médias de duração das reservas, por tipo de quarto e apenas para reservas finalizadas - com WITH
with 

--Médias de duração das reservas, por tipo de quarto e apenas para reservas finalizadas
medias_tipo_quarto as
(SELECT reserva.id_tipo_quarto, 
trunc(AVG (TRUNC(TO_DATE(reserva.data_fim, 'yyyy-mm-dd hh24:mi:ss')) - TRUNC(TO_DATE(reserva.data_inicio, 'yyyy-mm-dd hh24:mi:ss'))),2) as media -- calculo das médias
FROM reserva WHERE id_reserva IN (SELECT estadoreserva.id_reserva FROM estadoreserva where estadoreserva.id_estado = 3) -- qualquer médias apenas de reservas finalizadas
GROUP BY reserva.id_tipo_quarto),

--Todos os quartos reservados nos ultimos 6 meses e duracao da estadia
quartos_reservados_duracao as
(select numero_quarto || '-' || numero_andar as quarto, 
TRUNC(TO_DATE(reserva.data_fim, 'yyyy-mm-dd hh24:mi:ss')) - TRUNC(TO_DATE(reserva.data_inicio, 'yyyy-mm-dd hh24:mi:ss')) as duracao_estadia,
reserva.id_tipo_quarto,
estadoreserva.data
FROM reserva
inner join estadoreserva 
ON reserva.id_reserva = estadoreserva.id_reserva AND estadoreserva.id_estado = 3 AND estadoreserva.data >= add_months(sysdate, -6)),

--Intervenções de limpeza nos 6 meses anteriores à data atual, com data, quarto e camareira
intervences_limpeza_6_meses as
(select RegistoIntervencao.data_intervencao as data, RegistoIntervencao.numero_quarto || '-' || RegistoIntervencao.numero_andar as id_quarto, intervencaolimpeza.nif_funcionario as camareira
FROM RegistoIntervencao 
inner join intervencaolimpeza
ON RegistoIntervencao.id_intervencao = intervencaolimpeza.id_intervencao AND RegistoIntervencao.data_intervencao >= add_months(sysdate, -6)),

-- Quartos reservados nos ultimos 6 meses e que têm média de estadia superior à média do tipo de quarto
quartos_media_superior as
(select quartos_reservados_duracao.quarto as id_quarto, quartos_reservados_duracao.id_tipo_quarto, quartos_reservados_duracao.duracao_estadia, quartos_reservados_duracao.data
from quartos_reservados_duracao
inner join medias_tipo_quarto on quartos_reservados_duracao.id_tipo_quarto = medias_tipo_quarto.id_tipo_quarto and quartos_reservados_duracao.duracao_estadia > medias_tipo_quarto.media),

--select quartos_media_superior.id_quarto from quartos_media_superior;
--select * from quartos_media_superior;
--select * from meses;

limpeza_quartos_media_superior as --limpeza de quartos no ultimos 6 meses, em quartos com média superior do tipo de quarto
(select extract(month from intervences_limpeza_6_meses.data) as mes, intervences_limpeza_6_meses.id_quarto as id_quarto, intervences_limpeza_6_meses.camareira as camareira 

from intervences_limpeza_6_meses
inner join quartos_media_superior 
ON intervences_limpeza_6_meses.id_quarto = quartos_media_superior.id_quarto
)

select limpeza_quartos_media_superior.mes, limpeza_quartos_media_superior.camareira
from limpeza_quartos_media_superior
group by limpeza_quartos_media_superior.mes, limpeza_quartos_media_superior.camareira 
having count(*) = (select max(count(*)) from limpeza_quartos_media_superior FT where FT.mes=limpeza_quartos_media_superior.mes group by FT.mes,FT.camareira) order by 1 DESC;

--PARTE 3

--A
/*Apresentar por andar, o quarto e o tipo de quarto, que teve o maior número de
reservas. Deverão ser excluídos todos os quartos em que o número de reservas
é inferior a 2 e são do tipo “single�?. Não incluir reservas canceladas*/

select numero_andar, numero_quarto, tipoquarto.designacao, count(*) as total from reserva r
inner join tipoquarto on tipoquarto.id_tipo_quarto = r.id_tipo_quarto
where 
    numero_andar is not null 
    and r.id_tipo_quarto not in (
        select id_tipo_quarto from tipoquarto
        where designacao like 'Single'
    )       
group by numero_andar, numero_quarto, tipoquarto.designacao
having count(*) > 1 and count(*) = (
    select max(count(*)) from reserva r1
    inner join tipoquarto tq on r1.id_tipo_quarto = tq.id_tipo_quarto
    where 
        r1.numero_andar = r.numero_andar
        and r1.numero_quarto is not null
        and tq.designacao not like 'Single'
    group by numero_andar, numero_quarto
)
order by numero_andar; 

--B
/*Apresentar os clientes que ocuparam quartos do tipo suite na última época alta
e consumiram os dois produtos com maior consumo nos últimos dois anos. O
resultado deve ser ordenado por ordem decrescente do valor total do consumo*/

with

ultima_epoca_alta as (

    -- Traduzir a data das epocas para o ano atual e verificar se j� est� no 
    -- passado para ser usada essa ou a do ano anterior a essa
    select (case when
        add_months(data_inicio, (extract(YEAR from sysdate) - extract(YEAR from data_inicio)) * 12) > sysdate then add_months(data_inicio, (extract(YEAR from sysdate) - extract(YEAR from data_inicio) - 1) * 12)
        else  add_months(data_inicio, (extract(YEAR from sysdate) - extract(YEAR from data_inicio)) * 12)
        end) as data_inicio,
        (case when add_months(data_fim, (extract(YEAR from sysdate) - extract(YEAR from data_fim)) * 12) > sysdate then add_months(data_fim, (extract(YEAR from sysdate) - extract(YEAR from data_fim) - 1) * 12)
        else  add_months(data_fim, (extract(YEAR from sysdate) - extract(YEAR from data_fim)) * 12)
        end) as data_fim
    from epoca
    where designacao like 'Alta'
),
produtos_mais_consumidos as (
    select id_produto from consumo
    where consumo.data_consumo > sysdate - interval '2' year
    group by id_produto
    order by count(*) desc
    fetch first 2 rows only
)

select nif, sum(total) as total from (
    -- Clientes que consumiram ambos os produtos mais vendidos dos �ltimos dois anos
    select r.nif, c.id_produto, sum(c.quantidade) as total from consumo c
    inner join contaconsumo cc on cc.id_conta = c.id_conta
    inner join reserva r on r.id_reserva = cc.id_reserva
    right outer join produtos_mais_consumidos p on p.id_produto = c.id_produto
    where r.nif is not null
    group by r.nif, c.id_produto
    order by total
)
where nif in (
    -- Clientes que ocuparam quartos do tipo suite na �ltima �poca alta
    select nif from reserva, tipoquarto
    where 
        reserva.id_tipo_quarto = tipoquarto.id_tipo_quarto
        and tipoquarto.designacao like 'Suite'
        and data_inicio BETWEEN (select data_inicio from ultima_epoca_alta) and (select data_fim from ultima_epoca_alta)
)
group by nif
having count(*) = (select count(*) from produtos_mais_consumidos)
;
