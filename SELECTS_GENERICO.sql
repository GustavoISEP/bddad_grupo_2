SELECT numero_andar, nome 
FROM Andar;

SELECT nif_funcionario 
FROM Camareira;

SELECT nome, nif, email, telefone 
FROM Cliente;

SELECT id_conta, data_consumo, id_produto, quantidade, id_intervencao 
FROM Consumo;

SELECT data_abertura, id_conta, id_reserva 
FROM ContaConsumo;

SELECT designacao, data_inicio, data_fim 
FROM Epoca;

SELECT id_estado, descricao_estado 
FROM Estado;

SELECT id_reserva, id_estado, data 
FROM EstadoReserva;

SELECT id_fatura, valor_total, id_fatura_consumo, id_fatura_reserva 
FROM Fatura;

SELECT id_fatura, id_metodo_pagamento, valor_pago 
FROM Fatura_MetodoPagamento;

SELECT id_fatura_consumo, id_conta, valor 
FROM FaturaConsumo;

SELECT id_fatura_reserva, id_reserva, valor 
FROM FaturaReserva;

SELECT nif, nome, telefone, email 
FROM Funcionarios;

SELECT id_intervencao, nif_funcionario 
FROM IntervencaoLimpeza;

SELECT id_intervencao, nif_funcionario, descricao, id_pedido_intervencao 
FROM IntervencaoManutencao;

SELECT nif_funcionario, telefone_servico, id_responsavel 
FROM Manutencao;

SELECT id_metodo_pagamento, descricao 
FROM MetodoPagamento;

SELECT nif, rua, numero, localidade 
FROM Morada;

SELECT id_pedido_intervencao, data_pedido_intervencao, id_reserva 
FROM PedidoIntervencao;

SELECT id_produto, descricao_produto, id_tipo_produto, preco 
FROM Produto;

SELECT numero_quarto, lotacao, numero_andar, id_tipo_quarto 
FROM Quarto;

SELECT nif_funcionario 
FROM Rececao;

SELECT id_intervencao, data_intervencao, numero_quarto, numero_andar 
FROM RegistoIntervencao;

SELECT id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar 
FROM Reserva;

SELECT nif_funcionario 
FROM Restaurante;

SELECT id_tipo_produto, descricao_tipo 
FROM TipoProduto;

SELECT id_tipo_quarto, designacao 
FROM TipoQuarto;

SELECT designacao, id_tipo_quarto, preco 
FROM TipoQuarto_Epoca;

