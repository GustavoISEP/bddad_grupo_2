-- BEGIN
--Médias de duração das reservas, por tipo de quarto e apenas para reservas finalizadas - com WITH
with 

--Médias de duração das reservas, por tipo de quarto e apenas para reservas finalizadas
medias_tipo_quarto as
(SELECT reserva.id_tipo_quarto, 
trunc(AVG (TRUNC(TO_DATE(reserva.data_fim, 'yyyy-mm-dd hh24:mi:ss')) - TRUNC(TO_DATE(reserva.data_inicio, 'yyyy-mm-dd hh24:mi:ss'))),2) as media -- calculo das médias
FROM reserva WHERE id_reserva IN (SELECT estadoreserva.id_reserva FROM estadoreserva where estadoreserva.id_estado = 3) -- qualquer médias apenas de reservas finalizadas
GROUP BY reserva.id_tipo_quarto),

--Todos os quartos reservados nos ultimos 6 meses e duracao da estadia
quartos_reservados_duracao as
(select numero_quarto || '-' || numero_andar as quarto, 
TRUNC(TO_DATE(reserva.data_fim, 'yyyy-mm-dd hh24:mi:ss')) - TRUNC(TO_DATE(reserva.data_inicio, 'yyyy-mm-dd hh24:mi:ss')) as duracao_estadia,
reserva.id_tipo_quarto,
estadoreserva.data
FROM reserva
inner join estadoreserva 
ON reserva.id_reserva = estadoreserva.id_reserva AND estadoreserva.id_estado = 3 AND estadoreserva.data >= add_months(sysdate, -6)),

--Intervenções de limpeza nos 6 meses anteriores à data atual, com data, quarto e camareira
intervences_limpeza_6_meses as
(select RegistoIntervencao.data_intervencao as data, RegistoIntervencao.numero_quarto || '-' || RegistoIntervencao.numero_andar as id_quarto, intervencaolimpeza.nif_funcionario as camareira
FROM RegistoIntervencao 
inner join intervencaolimpeza
ON RegistoIntervencao.id_intervencao = intervencaolimpeza.id_intervencao AND RegistoIntervencao.data_intervencao >= add_months(sysdate, -6)),

--Obter os ultimos 6 meses da data atual (não considerei o mês atual porque não está"fechado")
meses as 
(select extract(month from sysdate)-Level AS mes from dual CONNECT BY Level<=6),

-- Quartos reservados nos ultimos 6 meses e que têm média de estadia superior à média do tipo de quarto
quartos_media_superior as
(select quartos_reservados_duracao.quarto as id_quarto, quartos_reservados_duracao.id_tipo_quarto, quartos_reservados_duracao.duracao_estadia, quartos_reservados_duracao.data
from quartos_reservados_duracao
inner join medias_tipo_quarto on quartos_reservados_duracao.id_tipo_quarto = medias_tipo_quarto.id_tipo_quarto and quartos_reservados_duracao.duracao_estadia > medias_tipo_quarto.media),

--select quartos_media_superior.id_quarto from quartos_media_superior;
--select * from quartos_media_superior;
--select * from meses;

limpeza_quartos_media_superior as --limpeza de quartos no ultimos 6 meses, em quartos com média superior do tipo de quarto
(select extract(month from intervences_limpeza_6_meses.data) as mes, intervences_limpeza_6_meses.id_quarto as id_quarto, intervences_limpeza_6_meses.camareira as camareira 

from intervences_limpeza_6_meses
inner join quartos_media_superior 
ON intervences_limpeza_6_meses.id_quarto = quartos_media_superior.id_quarto
),

finaltable as -- select com registo de quantas limpezas fez cada camareira em cada mês, nos quartos com média superior e nos ultimos 6 meses
(select limpeza_quartos_media_superior.mes, limpeza_quartos_media_superior.camareira as camareira, count (limpeza_quartos_media_superior.camareira) as total
from limpeza_quartos_media_superior
GROUP BY limpeza_quartos_media_superior.mes, limpeza_quartos_media_superior.camareira
)


select limpeza_quartos_media_superior.mes, limpeza_quartos_media_superior.camareira, count(*) as total 
from limpeza_quartos_media_superior
group by limpeza_quartos_media_superior.mes, limpeza_quartos_media_superior.camareira 
having count(*) = (select max(count(*)) from limpeza_quartos_media_superior FT where FT.mes=limpeza_quartos_media_superior.mes group by FT.mes,FT.camareira) order by 1 DESC;


--END