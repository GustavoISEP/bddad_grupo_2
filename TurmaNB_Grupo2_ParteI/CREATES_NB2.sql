-- ** eliminar tabelas se existentes **
-- CASCADE CONSTRAINTS para eliminar as restri��es de integridade das chaves prim�rias e chaves �nicas
-- PURGE elimina a tabela da base de dados e da "reciclagem"
DROP TABLE Andar                    CASCADE CONSTRAINTS PURGE;
DROP TABLE Camareira                CASCADE CONSTRAINTS PURGE;
DROP TABLE Cliente                  CASCADE CONSTRAINTS PURGE;
DROP TABLE Consumo                  CASCADE CONSTRAINTS PURGE;
DROP TABLE ContaConsumo             CASCADE CONSTRAINTS PURGE;
DROP TABLE Epoca                    CASCADE CONSTRAINTS PURGE;
DROP TABLE Estado                   CASCADE CONSTRAINTS PURGE;
DROP TABLE EstadoReserva            CASCADE CONSTRAINTS PURGE;
DROP TABLE Fatura                   CASCADE CONSTRAINTS PURGE;
DROP TABLE Fatura_MetodoPagamento   CASCADE CONSTRAINTS PURGE;
DROP TABLE FaturaConsumo            CASCADE CONSTRAINTS PURGE;
DROP TABLE FaturaReserva            CASCADE CONSTRAINTS PURGE;
DROP TABLE Funcionarios             CASCADE CONSTRAINTS PURGE;
DROP TABLE IntervencaoLimpeza       CASCADE CONSTRAINTS PURGE;
DROP TABLE IntervencaoManutencao    CASCADE CONSTRAINTS PURGE;
DROP TABLE Manutencao               CASCADE CONSTRAINTS PURGE;
DROP TABLE MetodoPagamento          CASCADE CONSTRAINTS PURGE;
DROP TABLE Concelho                 CASCADE CONSTRAINTS PURGE;
DROP TABLE Localidade               CASCADE CONSTRAINTS PURGE;
DROP TABLE PedidoIntervencao        CASCADE CONSTRAINTS PURGE;
DROP TABLE Produto                  CASCADE CONSTRAINTS PURGE;
DROP TABLE Quarto                   CASCADE CONSTRAINTS PURGE;
DROP TABLE Rececao                  CASCADE CONSTRAINTS PURGE;
DROP TABLE RegistoIntervencao       CASCADE CONSTRAINTS PURGE;
DROP TABLE Reserva                  CASCADE CONSTRAINTS PURGE;
DROP TABLE Restaurante              CASCADE CONSTRAINTS PURGE;
DROP TABLE TipoProduto              CASCADE CONSTRAINTS PURGE;
DROP TABLE TipoQuarto               CASCADE CONSTRAINTS PURGE;
DROP TABLE TipoQuarto_Epoca         CASCADE CONSTRAINTS PURGE;


-- ** criar tabelas **
CREATE TABLE Andar (
    numero_andar    INTEGER         CONSTRAINT pkAndarNumero        PRIMARY KEY, 
    nome            VARCHAR(40)     CONSTRAINT nnAndarNome          NOT NULL
);

CREATE TABLE Camareira (
    nif_funcionario     INTEGER     CONSTRAINT pkCamareiraNif       PRIMARY KEY
									CONSTRAINT ckCamareiraNif		CHECK(REGEXP_LIKE(nif_funcionario, '^\d{9}$'))
);

CREATE TABLE Cliente (
    nome            VARCHAR(40)     CONSTRAINT nnClienteNome        NOT NULL, 
    nif             INTEGER         CONSTRAINT pkClienteNif         PRIMARY KEY
									CONSTRAINT ckClienteNif	        CHECK(REGEXP_LIKE(nif, '^\d{9}$')),
    email           VARCHAR(60), 
    telefone        INTEGER,
    rua             VARCHAR(75)     CONSTRAINT nnRuaCliente             NOT NULL,
    id_localidade   INTEGER         CONSTRAINT nnIDLocalidadeCliente    NOT NULL
);

CREATE TABLE Consumo (
    id_conta        INTEGER, 
    data_consumo    TIMESTAMP, 
    id_produto      INTEGER, 
    quantidade      INTEGER         CONSTRAINT nnConsumoQuantidade  NOT NULL, 
    id_intervencao  INTEGER         CONSTRAINT nnConsumoIntervencao NOT NULL,
    
    CONSTRAINT pkConsumoContaDataProduto PRIMARY KEY (id_conta, data_consumo, id_produto)
);
    
CREATE TABLE ContaConsumo (
    data_hora_abertura  TIMESTAMP       CONSTRAINT nnContaConsumoDataHora   NOT NULL, 
    id_conta            INTEGER         CONSTRAINT pkContaConsumoConta      PRIMARY KEY, 
    id_reserva          INTEGER         CONSTRAINT nnContaConsumoReserva    NOT NULL
);

CREATE TABLE Epoca (
    designacao      VARCHAR(40)         CONSTRAINT pkEpocaDesignacao        PRIMARY KEY, 
    data_inicio     date                CONSTRAINT nnEpocaDataInicio        NOT NULL, 
    data_fim        date                CONSTRAINT nnEpocaDataFim           NOT NULL
);

CREATE TABLE Estado (
    id_estado           INTEGER         CONSTRAINT pkEstadoID               PRIMARY KEY, 
    descricao_estado    VARCHAR(20)     CONSTRAINT nnEstadoDescricao        NOT NULL 
                                        CONSTRAINT ukEstadoDescricao        UNIQUE
);

CREATE TABLE EstadoReserva (
    id_reserva  INTEGER, 
    id_estado   INTEGER, 
    data        TIMESTAMP,
    
    CONSTRAINT pkEstadoReservaIDReservaIDEstadoData PRIMARY KEY (id_reserva, id_estado, data)
);

CREATE TABLE Fatura (
    id_fatura               INTEGER             CONSTRAINT pkFaturaID               PRIMARY KEY,
    valor_total             NUMERIC(8, 2)       CONSTRAINT nnFaturaValorTotal       NOT NULL, 
    id_fatura_consumo       INTEGER             CONSTRAINT nnFaturaConsumo          NOT NULL, 
    id_fatura_reserva       INTEGER             CONSTRAINT nnFaturaReserva          NOT NULL
);

CREATE TABLE Fatura_MetodoPagamento (
    id_fatura                   INTEGER, 
    id_metodo_pagamento         INTEGER, 
    valor_pago                  NUMERIC(8, 2)   CONSTRAINT nnFaturaMPagamentoValorPago      NOT NULL,
    
    CONSTRAINT pkFaturaMPagamentoIDFaturaIDMPagamento PRIMARY KEY (id_fatura, id_metodo_pagamento)
);

CREATE TABLE FaturaConsumo (
    id_fatura_consumo   INTEGER             CONSTRAINT pkFaturaConsumoIDFaturaConsumo           PRIMARY KEY,
    id_conta            INTEGER             CONSTRAINT nnFaturaConsumoIDConta                   NOT NULL, 
    valor               NUMERIC(8, 2)       CONSTRAINT nnFaturaConsumoValor                     NOT NULL
);

CREATE TABLE FaturaReserva (
    id_fatura_reserva   INTEGER             CONSTRAINT pkFaturaReservaID            PRIMARY KEY, 
    id_reserva          INTEGER             CONSTRAINT nnFaturaReservaIDReserva     NOT NULL, 
    valor               NUMERIC(8, 2)       CONSTRAINT nnFaturaReservaValor         NOT NULL
);

CREATE TABLE Funcionarios (
    nif INTEGER         CONSTRAINT pkFuncionariosNif            PRIMARY KEY
						CONSTRAINT ckFuncionariosNif			CHECK(REGEXP_LIKE(nif, '^\d{9}$')),
    nome VARCHAR(40)    CONSTRAINT nnFuncionariosNome           NOT NULL, 
    telefone INTEGER    CONSTRAINT nnFuncionariosTelefone       NOT NULL
						CONSTRAINT ckFuncionariosTelefone		CHECK(REGEXP_LIKE(telefone, '^\d{9}$')), 
    email VARCHAR(60)   CONSTRAINT nnFuncionariosEmail          NOT NULL,
    rua             VARCHAR(75)     CONSTRAINT nnRuaFuncionarios            NOT NULL,
    id_localidade   INTEGER         CONSTRAINT nnIDLocalidadeFuncionarios    NOT NULL
);
 
CREATE TABLE IntervencaoLimpeza (
    id_intervencao  INTEGER     CONSTRAINT pkIntervencaoLimpezaIdIntervencao    PRIMARY KEY, 
    nif_funcionario INTEGER     CONSTRAINT nnIntervencaoLimpezaNifFuncionario   NOT NULL
    
	CONSTRAINT ckIntervencaoLimpezaNifFuncionario	CHECK(REGEXP_LIKE(nif_funcionario, '^\d{9}$'))
);
 
CREATE TABLE IntervencaoManutencao (
    id_intervencao          INTEGER     CONSTRAINT pkIntervencaoManutencaoIdIntervencao     PRIMARY KEY, 
    nif_funcionario         INTEGER     CONSTRAINT nnIntervencaoManutencaoNifFuncionario    NOT NULL
                                        CONSTRAINT ckIntervencaoManutencaoNifFuncionario	CHECK(REGEXP_LIKE(nif_funcionario, '^\d{9}$')), 
    descricao               VARCHAR(60) CONSTRAINT nnIntervencaoManutencaoDescricao         NOT NULL, 
    id_pedido_intervencao   INTEGER
);
 
CREATE TABLE Manutencao (
    nif_funcionario     INTEGER     CONSTRAINT pkManutencaoNifFuncionario       PRIMARY KEY
                                    CONSTRAINT ckManutencaoNifFuncionario		CHECK(REGEXP_LIKE(nif_funcionario, '^\d{9}$')),
    telefone_servico    INTEGER     CONSTRAINT nnManutencaoTelefoneServico      NOT NULL, 
    id_responsavel      INTEGER
);
 
CREATE TABLE MetodoPagamento (
    id_metodo_pagamento INTEGER     CONSTRAINT pkMetodoPagamentoIdMetodoPagamento   PRIMARY KEY, 
    descricao           VARCHAR(40) CONSTRAINT nnMetodoPagamentodescricao           NOT NULL
);

CREATE TABLE Localidade (
    id_localidade INTEGER               CONSTRAINT pkLocalidadeID           PRIMARY KEY,
    descricao_localidade VARCHAR(50)    CONSTRAINT nnDescricaoLocalidade    NOT NULL,
    id_concelho INTEGER                 CONSTRAINT nnConselhoID             NOT NULL
);

CREATE TABLE Concelho (
    id_concelho INTEGER                 CONSTRAINT pkConselhoID             PRIMARY KEY,
    descricao_concelho VARCHAR(50)      CONSTRAINT nnDescricaoConcelho    NOT NULL
);
 
CREATE TABLE PedidoIntervencao (
    id_pedido_intervencao   INTEGER         CONSTRAINT pkPedidoIntervencaoIdPedidoIntervencao       PRIMARY KEY, 
    data_pedido_intervencao timestamp    	CONSTRAINT nnPedidoIntervencaoDataPedidoIntervencao     NOT NULL, 
    id_reserva              INTEGER         CONSTRAINT nnPedidoIntervencaoIdReserva                 NOT NULL
);
 
CREATE TABLE Produto (
    id_produto          INTEGER         CONSTRAINT pkProdutoIdProduto           PRIMARY KEY, 
    descricao_produto   VARCHAR(40)     CONSTRAINT nnProdutoDescricaoProduto    NOT NULL
                                        CONSTRAINT ukProdutoDescricaoProduto    UNIQUE,
    id_tipo_produto     INTEGER         CONSTRAINT nnProdutoIdTipoProduto       NOT NULL, 
    preco               NUMERIC(5, 2)   CONSTRAINT nnProdutopreco               NOT NULL
);
 
CREATE TABLE Quarto (
    numero_quarto   INTEGER, 
    lotacao         INTEGER     CONSTRAINT nnQuartoLotacao                      NOT NULL, 
    numero_andar    INTEGER, 
    id_tipo_quarto  INTEGER     CONSTRAINT nnQuartoIdTipoQuarto                 NOT NULL,    

	CONSTRAINT pkQuartoNumeroQuartoNumeroAndar      PRIMARY KEY (numero_quarto, numero_andar)
);
 
CREATE TABLE Rececao (
    nif_funcionario INTEGER     CONSTRAINT pkRececaoNifFuncionario      PRIMARY KEY
									CONSTRAINT ckRececaoNif		CHECK(REGEXP_LIKE(nif_funcionario, '^\d{9}$'))
);

CREATE TABLE RegistoIntervencao (
    id_intervencao      INTEGER     CONSTRAINT pkRegistoIntervencaoIdIntervencao    PRIMARY KEY,
    data_intervencao    TIMESTAMP,
    numero_quarto       INTEGER     CONSTRAINT nnNumeroQuarto                       NOT NULL,
    numero_andar        INTEGER     CONSTRAINT nnNumeroAndar                        NOT NULL
);

CREATE TABLE Reserva (
    id_reserva      INTEGER     CONSTRAINT pkReservaId              PRIMARY KEY,
    nif             INTEGER     CONSTRAINT nnReservaNif             NOT NULL
								CONSTRAINT ckReservaNif		CHECK(REGEXP_LIKE(nif, '^\d{9}$')),
    id_tipo_quarto  INTEGER     CONSTRAINT nnReservaTipoQuarto      NOT NULL,
    numero_pessoas  INTEGER     CONSTRAINT nnReservaNumeroPessoas   NOT NULL,
    data_inicio     TIMESTAMP,
    data_fim        TIMESTAMP   CONSTRAINT nnReservaDataFim         NOT NULL,
    numero_quarto   INTEGER,
    numero_andar    INTEGER,

    CONSTRAINT  ukReservaNifDataInicioNumeroQuartoNumeroAndar   UNIQUE(nif, data_inicio, numero_quarto, numero_andar)
);

CREATE TABLE Restaurante (
    nif_funcionario     INTEGER     CONSTRAINT pkRestauranteId  PRIMARY KEY
									CONSTRAINT ckRestauranteNif		CHECK(REGEXP_LIKE(nif_funcionario, '^\d{9}$'))
);

CREATE TABLE TipoProduto (
    id_tipo_produto     INTEGER         CONSTRAINT pkTipoProdutoId              PRIMARY KEY,
    descricao_tipo      VARCHAR(40)     CONSTRAINT nnTipoProdutoDescricaoTipo   NOT NULL 
                                        CONSTRAINT ukTipoProdutoDescricaoTipo   UNIQUE
);

CREATE TABLE TipoQuarto (
    id_tipo_quarto      INTEGER         CONSTRAINT pkTipoQuartoId           PRIMARY KEY,
    designacao          VARCHAR(50)     CONSTRAINT nnTipoQuartoDesignacao    NOT NULL
);

CREATE TABLE TipoQuarto_Epoca (
    designacao          VARCHAR(40),
    id_tipo_quarto      INTEGER,
    preco               NUMERIC(5, 2) CONSTRAINT nnTipoQuartoEpocaPreco  NOT NULL,

    CONSTRAINT pkTipoQuartoEpocaDesignacao  PRIMARY KEY (designacao, id_tipo_quarto)
);

-- ** alterar tabelas **
ALTER TABLE Camareira                       ADD CONSTRAINT FKCamareira_Funcionarios                         FOREIGN KEY (nif_funcionario)                   REFERENCES Funcionarios (nif);
ALTER TABLE Cliente                         ADD CONSTRAINT FKCliente_Localidade                             FOREIGN KEY (id_localidade)                     REFERENCES Localidade (id_localidade);
ALTER TABLE Consumo                         ADD CONSTRAINT FKConsumoConta_Consumo                           FOREIGN KEY (id_conta)                          REFERENCES ContaConsumo (id_conta);
ALTER TABLE Consumo                         ADD CONSTRAINT FKConsumo_IntervencaoLimpeza                     FOREIGN KEY (id_intervencao)                    REFERENCES IntervencaoLimpeza (id_intervencao);
ALTER TABLE Consumo                         ADD CONSTRAINT FKConsumo_Produto                                FOREIGN KEY (id_produto)                        REFERENCES Produto (id_produto);
ALTER TABLE ContaConsumo                    ADD CONSTRAINT FKContaConsumo_Reserva                           FOREIGN KEY (id_reserva)                        REFERENCES Reserva (id_reserva);
ALTER TABLE EstadoReserva                   ADD CONSTRAINT FKEstadoReserva_Estado                           FOREIGN KEY (id_estado)                         REFERENCES Estado (id_estado);
ALTER TABLE EstadoReserva                   ADD CONSTRAINT FKEstadoReserva_Reserva                          FOREIGN KEY (id_reserva)                        REFERENCES Reserva (id_reserva);
ALTER TABLE Fatura                          ADD CONSTRAINT FKFatura_FaturaReserva                           FOREIGN KEY (id_fatura_reserva)                 REFERENCES FaturaReserva (id_fatura_reserva);
ALTER TABLE Fatura                          ADD CONSTRAINT FKFatura_FaturaConsumo                           FOREIGN KEY (id_fatura_consumo)                 REFERENCES FaturaConsumo (id_fatura_consumo);
ALTER TABLE Fatura_MetodoPagamento          ADD CONSTRAINT FKFaturaMP_MetodoPagamento                       FOREIGN KEY (id_metodo_pagamento)               REFERENCES MetodoPagamento (id_metodo_pagamento);
ALTER TABLE Fatura_MetodoPagamento          ADD CONSTRAINT FKFaturaMP_Fatura                                FOREIGN KEY (id_fatura)                         REFERENCES Fatura (id_fatura);
ALTER TABLE FaturaConsumo                   ADD CONSTRAINT FKFaturaConsumo_ContaConsumo                     FOREIGN KEY (id_conta)                          REFERENCES ContaConsumo (id_conta);
ALTER TABLE FaturaReserva                   ADD CONSTRAINT FKFaturaReserva_Reserva                          FOREIGN KEY (id_reserva)                        REFERENCES Reserva (id_reserva);
ALTER TABLE Funcionarios                    ADD CONSTRAINT FKFuncionarios_Localidade                        FOREIGN KEY (id_localidade)                     REFERENCES Localidade (id_localidade);
ALTER TABLE IntervencaoLimpeza              ADD CONSTRAINT FKIntervencaoLimpeza_RegistoIntervencao          FOREIGN KEY (id_intervencao)                    REFERENCES RegistoIntervencao (id_intervencao);
ALTER TABLE IntervencaoLimpeza              ADD CONSTRAINT FKIntervencaoLimpeza_Camareira                   FOREIGN KEY (nif_funcionario)                   REFERENCES Camareira (nif_funcionario);
ALTER TABLE IntervencaoManutencao           ADD CONSTRAINT FKIntervencaoManutencao_Manutencao               FOREIGN KEY (nif_funcionario)                   REFERENCES Manutencao (nif_funcionario);
ALTER TABLE IntervencaoManutencao           ADD CONSTRAINT FKIntervencaoManutencao_RegistoIntervencao       FOREIGN KEY (id_intervencao)                    REFERENCES RegistoIntervencao (id_intervencao);
ALTER TABLE IntervencaoManutencao           ADD CONSTRAINT FKIntervencaoManutencao_PedidoIntervencao        FOREIGN KEY (id_pedido_intervencao)             REFERENCES PedidoIntervencao (id_pedido_intervencao);
ALTER TABLE Manutencao                      ADD CONSTRAINT FKManutencao_Funcionarios                        FOREIGN KEY (nif_funcionario)                   REFERENCES Funcionarios (nif);
ALTER TABLE Manutencao                      ADD CONSTRAINT FKManutencao_Manutencao                          FOREIGN KEY (id_responsavel)                    REFERENCES Manutencao (nif_funcionario);
ALTER TABLE Localidade                      ADD CONSTRAINT FKLocalidade_Concelho                            FOREIGN KEY (id_concelho)                       REFERENCES Concelho (id_concelho);
ALTER TABLE PedidoIntervencao               ADD CONSTRAINT FKPedidoIntervencao_Reserva                      FOREIGN KEY (id_reserva)                        REFERENCES Reserva (id_reserva);
ALTER TABLE Produto                         ADD CONSTRAINT FKProduto_TipoProduto                            FOREIGN KEY (id_tipo_produto)                   REFERENCES TipoProduto (id_tipo_produto);
ALTER TABLE Quarto                          ADD CONSTRAINT FKQuarto_TipoQuarto                              FOREIGN KEY (id_tipo_quarto)                    REFERENCES TipoQuarto (id_tipo_quarto);
ALTER TABLE Quarto                          ADD CONSTRAINT FKQuarto_Andar                                   FOREIGN KEY (numero_andar)                      REFERENCES Andar (numero_andar);
ALTER TABLE Rececao                         ADD CONSTRAINT FKRececao_Funcionarios                           FOREIGN KEY (nif_funcionario)                   REFERENCES Funcionarios (nif);
ALTER TABLE RegistoIntervencao              ADD CONSTRAINT FKRegistoIntervencao_Quarto                      FOREIGN KEY (numero_quarto, numero_andar)       REFERENCES Quarto (numero_quarto, numero_andar);
ALTER TABLE Reserva                         ADD CONSTRAINT FKReserva_TipoQuarto                             FOREIGN KEY (id_tipo_quarto)                    REFERENCES TipoQuarto (id_tipo_quarto);
ALTER TABLE Reserva                         ADD CONSTRAINT FKReserva_Cliente                                FOREIGN KEY (nif)                               REFERENCES Cliente (nif);
ALTER TABLE Reserva                         ADD CONSTRAINT FKReserva_Quarto                                 FOREIGN KEY (numero_quarto, numero_andar)       REFERENCES Quarto (numero_quarto, numero_andar);
ALTER TABLE Restaurante                     ADD CONSTRAINT FKRestaurante_Funcionarios                       FOREIGN KEY (nif_funcionario)                   REFERENCES Funcionarios (nif);
ALTER TABLE TipoQuarto_Epoca                ADD CONSTRAINT FKTipoQuarto_Epoca                               FOREIGN KEY (designacao)                        REFERENCES Epoca (designacao);
ALTER TABLE TipoQuarto_Epoca                ADD CONSTRAINT FKTipoQuarto_TipoQuarto                          FOREIGN KEY (id_tipo_quarto)                    REFERENCES TipoQuarto (id_tipo_quarto);