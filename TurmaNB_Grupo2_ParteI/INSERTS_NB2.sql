-- INSERT INTO Andar(numero_andar, nome) VALUES (?, ?);
INSERT INTO Andar(numero_andar, nome) VALUES (1, 'Piso 1');
INSERT INTO Andar(numero_andar, nome) VALUES (2, 'Piso 2');
INSERT INTO Andar(numero_andar, nome) VALUES (3, 'Piso 3');

-- INSERT INTO Concelho(id_concelho, descricao_concelho) VALUES (?, ?);
INSERT INTO Concelho(id_concelho, descricao_concelho) VALUES (1, 'Porto');
INSERT INTO Concelho(id_concelho, descricao_concelho) VALUES (2, 'Vila Real');
INSERT INTO Concelho(id_concelho, descricao_concelho) VALUES (3, 'Braga');
INSERT INTO Concelho(id_concelho, descricao_concelho) VALUES (4, 'Lisboa');
INSERT INTO Concelho(id_concelho, descricao_concelho) VALUES (5, 'Bragança');
INSERT INTO Concelho(id_concelho, descricao_concelho) VALUES (6, 'Viana do Castelo');
INSERT INTO Concelho(id_concelho, descricao_concelho) VALUES (7, 'Guimarães');

-- INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (?, ?, ?);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (1, 'Porto', 1);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (2, 'Lisboa', 4);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (3, 'Ameixoeira', 4);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (4, 'Galegos', 2);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (5, 'Mascoselo', 2);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (6, 'Vila Real', 2);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (7, 'Amares', 3);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (8, 'Braga', 3);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (9, 'Gualtar', 3);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (10, 'Aveleda', 5);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (11, 'Bragança', 5);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (12, 'Viana do Castelo', 6);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (13, 'Nina', 6);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (14, 'Guimarães', 7);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (15, 'Mesão Frio', 7);
INSERT INTO Localidade(id_localidade, descricao_localidade, id_concelho) VALUES (16, 'Paranhos', 1);

-- INSERT INTO Funcionarios(nif, nome, telefone, email, rua, id_localidade) VALUES (?, ?, ?, ?, ?, ?);
INSERT INTO Funcionarios(nif, nome, telefone, email, rua, id_localidade) VALUES (666777888, 'Jo�o', 912345678, 'joao678@email.pt', 'Rua 25 de Abril, 456',1);
INSERT INTO Funcionarios(nif, nome, telefone, email, rua, id_localidade) VALUES (777888999, 'Manuel', 931245678, 'manuel789@email.pt', 'Rua Augusta, 702',3);
INSERT INTO Funcionarios(nif, nome, telefone, email, rua, id_localidade) VALUES (888999000, 'Ana', 961234578, 'ana890@email.pt', 'Rua da Travessa, 2',4);
INSERT INTO Funcionarios(nif, nome, telefone, email, rua, id_localidade) VALUES (999000111, '®gela', 921345678, 'angela901@email.pt', 'Rua dos Matos, 48',2);
INSERT INTO Funcionarios(nif, nome, telefone, email, rua, id_localidade) VALUES (123456789, 'Catarina', 910234567, 'catarina123@email.pt', 'Rua Engenheiro Pedro Batista, 1',5);
INSERT INTO Funcionarios(nif, nome, telefone, email, rua, id_localidade) VALUES (234890567, 'Sofia', 930234567, 'sofia123@email.pt', 'Rua do Jardim Publico, 12 - RC Direito',1);
INSERT INTO Funcionarios(nif, nome, telefone, email, rua, id_localidade) VALUES (567123098, 'Joana', 960234567, 'joana123@email.pt', 'Travessa Jo㯠Segundo, 335',8);

-- INSERT INTO Camareira(nif_funcionario) VALUES (?);
INSERT INTO Camareira(nif_funcionario) VALUES (888999000);
INSERT INTO Camareira(nif_funcionario) VALUES (999000111);
INSERT INTO Camareira(nif_funcionario) VALUES (123456789);
INSERT INTO Camareira(nif_funcionario) VALUES (234890567);
INSERT INTO Camareira(nif_funcionario) VALUES (567123098);

-- INSERT INTO Manutencao(nif_funcionario, telefone_servico, id_responsavel) VALUES (?, ?, ?);
INSERT INTO Manutencao(nif_funcionario, telefone_servico, id_responsavel) VALUES (777888999, 962358271, null);
INSERT INTO Manutencao(nif_funcionario, telefone_servico, id_responsavel) VALUES (666777888, 912155779, 777888999);

-- INSERT INTO Restaurante(nif_funcionario) VALUES (?);

-- INSERT INTO Rececao(nif_funcionario) VALUES (?);

-- INSERT INTO Cliente(nome, nif, email, telefone) VALUES (?, ?, ?, ?, ?, ?);
INSERT INTO Cliente(nome, nif, email, telefone, rua, id_localidade) VALUES ('Carlos', 111222333, 'carlos123@email.pt', 252111222, 'Rua Bernardino Machado, 496',7);
INSERT INTO Cliente(nome, nif, email, telefone, rua, id_localidade) VALUES ('Tiago', 222333444, 'tiago234@email.pt', 253444333, 'Rua da Liberdade, 205',1);
INSERT INTO Cliente(nome, nif, email, telefone, rua, id_localidade) VALUES ('Gustavo', 333444555, 'gustavo345@email.pt', 251555666, 'Rua 5 de Outubro, 12 - 1º Direito',16);
INSERT INTO Cliente(nome, nif, email, telefone, rua, id_localidade) VALUES ('Bruno', 444555666, 'bruno456@email.pt', 252888999, 'Rua Lu�?�e Camões',6);
INSERT INTO Cliente(nome, nif, email, telefone, rua, id_localidade) VALUES ('Maria', 555666777, 'maria567@email.pt', 252777888, 'Rua 25 de Abril, 109',5);
INSERT INTO Cliente(nome, nif, email, telefone, rua, id_localidade) VALUES ('Joana', 666778788, 'joana234@email.pt', 228884637, 'Rua 25 de Abril, 456',1);
INSERT INTO Cliente(nome, nif, email, telefone, rua, id_localidade) VALUES ('Manuel', 777889899, 'manuel345@email.pt', 219184632, 'Rua Augusta, 702',3);
INSERT INTO Cliente(nome, nif, email, telefone, rua, id_localidade) VALUES ('Ricardo', 888999111, 'ricardo456@email.pt', 918826375, 'Rua do Comercio, 100',8);
INSERT INTO Cliente(nome, nif, email, telefone, rua, id_localidade) VALUES ('Sofia', 999111222, 'sofia567@email.pt', 963385628, 'Rua do Carmo, 100',14);
INSERT INTO Cliente(nome, nif, email, telefone, rua, id_localidade) VALUES ('José Silva', 554785485, 'jose_silva@email.pt', 254854587, 'Rua das Arvores, 100 - RC Esquesdo',4);
INSERT INTO Cliente(nome, nif, email, telefone, rua, id_localidade) VALUES ('Antonio Costa', 547854125, 'costa1990@email.pt', 256878545, 'Rua Nova, 25',1);
INSERT INTO Cliente(nome, nif, email, telefone, rua, id_localidade) VALUES ('Cristiano Ronaldo', 117854873, 'cris_cris@email.pt', 954525745, 'Rua da Ponto, 1',1);
INSERT INTO Cliente(nome, nif, email, telefone, rua, id_localidade) VALUES ('Marcelo de Sousa', 217854873, 'marcelao@email.pt', 934515745, 'Rua dos Castros, 1',14);

-- INSERT INTO Epoca(designacao, data_inicio, data_fim) VALUES (?, ? ?);
INSERT INTO Epoca(designacao, data_inicio, data_fim) VALUES ('Alta', TO_DATE('2021-06-01','yyyy-mm-dd'), TO_DATE('2021-09-30','yyyy-mm-dd'));
INSERT INTO Epoca(designacao, data_inicio, data_fim) VALUES ('Media', TO_DATE('2021-02-01','yyyy-mm-dd'), TO_DATE('2021-05-31','yyyy-mm-dd'));
INSERT INTO Epoca(designacao, data_inicio, data_fim) VALUES ('Baixa', TO_DATE('2021-10-01','yyyy-mm-dd'), TO_DATE('2021-01-31','yyyy-mm-dd'));

-- INSERT INTO TipoQuarto(id_tipo_quarto, designacao) VALUES (?, ?);
INSERT INTO TipoQuarto(id_tipo_quarto, designacao) VALUES (1, 'Single');
INSERT INTO TipoQuarto(id_tipo_quarto, designacao) VALUES (2, 'Twin');
INSERT INTO TipoQuarto(id_tipo_quarto, designacao) VALUES (3, 'Superior');
INSERT INTO TipoQuarto(id_tipo_quarto, designacao) VALUES (4, 'Suite');

-- INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES (?, ?, ?);

-- INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (?, ?, ?, ?);
-- Quartos Piso 1
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (1, 2, 1, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (2, 2, 1, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (3, 2, 1, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (4, 2, 1, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (5, 2, 1, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (6, 2, 1, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (7, 2, 1, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (8, 2, 1, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (9, 2, 1, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (10, 2, 1, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (11, 2, 1, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (12, 2, 1, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (13, 2, 1, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (14, 2, 1, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (15, 2, 1, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (16, 2, 1, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (17, 2, 1, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (18, 2, 1, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (19, 2, 1, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (20, 2, 1, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (21, 3, 1, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (22, 3, 1, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (23, 3, 1, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (24, 3, 1, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (25, 3, 1, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (26, 3, 1, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (27, 3, 1, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (28, 3, 1, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (29, 3, 1, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (30, 3, 1, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (31, 4, 1, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (32, 4, 1, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (33, 4, 1, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (34, 4, 1, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (35, 4, 1, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (36, 4, 1, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (37, 4, 1, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (38, 4, 1, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (39, 4, 1, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (40, 4, 1, 4);

-- Quartos Piso 2
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (1, 2, 2, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (2, 2, 2, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (3, 2, 2, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (4, 2, 2, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (5, 2, 2, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (6, 2, 2, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (7, 2, 2, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (8, 2, 2, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (9, 2, 2, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (10, 2, 2, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (11, 2, 2, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (12, 2, 2, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (13, 2, 2, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (14, 2, 2, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (15, 2, 2, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (16, 2, 2, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (17, 2, 2, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (18, 2, 2, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (19, 2, 2, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (20, 2, 2, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (21, 3, 2, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (22, 3, 2, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (23, 3, 2, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (24, 3, 2, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (25, 3, 2, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (26, 3, 2, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (27, 3, 2, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (28, 3, 2, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (29, 3, 2, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (30, 3, 2, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (31, 4, 2, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (32, 4, 2, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (33, 4, 2, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (34, 4, 2, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (35, 4, 2, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (36, 4, 2, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (37, 4, 2, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (38, 4, 2, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (39, 4, 2, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (40, 4, 2, 4);

-- Quartos Piso 3
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (1, 2, 3, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (2, 2, 3, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (3, 2, 3, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (4, 2, 3, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (5, 2, 3, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (6, 2, 3, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (7, 2, 3, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (8, 2, 3, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (9, 2, 3, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (10, 2, 3, 1);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (11, 2, 3, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (12, 2, 3, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (13, 2, 3, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (14, 2, 3, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (15, 2, 3, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (16, 2, 3, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (17, 2, 3, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (18, 2, 3, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (19, 2, 3, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (20, 2, 3, 2);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (21, 3, 3, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (22, 3, 3, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (23, 3, 3, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (24, 3, 3, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (25, 3, 3, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (26, 3, 3, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (27, 3, 3, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (28, 3, 3, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (29, 3, 3, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (30, 3, 3, 3);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (31, 4, 3, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (32, 4, 3, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (33, 4, 3, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (34, 4, 3, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (35, 4, 3, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (36, 4, 3, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (37, 4, 3, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (38, 4, 3, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (39, 4, 3, 4);
INSERT INTO Quarto(numero_quarto, lotacao, numero_andar, id_tipo_quarto) VALUES (40, 4, 3, 4);

-- INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES (?, ?, ?);
-- Época Baixa
INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES ('Baixa', 1, 70.0);
INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES ('Baixa', 2, 80.0);
INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES ('Baixa', 3, 90.0);
INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES ('Baixa', 4, 100.0);

-- Época Média
INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES ('Media', 1, 90.0);
INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES ('Media', 2, 100.0);
INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES ('Media', 3, 110.0);
INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES ('Media', 4, 120.0);

-- Época Alta
INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES ('Alta', 1, 110.0);
INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES ('Alta', 2, 120.0);
INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES ('Alta', 3, 130.0);
INSERT INTO TipoQuarto_Epoca(designacao, id_tipo_quarto, preco) VALUES ('Alta', 4, 140.0);

-- INSERT INTO Estado(id_estado, descricao_estado) VALUES (?, ?);
INSERT INTO Estado(id_estado, descricao_estado) VALUES (1, 'Reservada');
INSERT INTO Estado(id_estado, descricao_estado) VALUES (2, 'Ativa');
INSERT INTO Estado(id_estado, descricao_estado) VALUES (3, 'Finalizada');
INSERT INTO Estado(id_estado, descricao_estado) VALUES (4, 'Cancelada');

-- Reservas
-- INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) VALUES (?, ?, ?, ?, ?, ?, ?, ?);
-- INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (?, ?, ?);

-- Reservas �poca Alta
INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (1, 111222333, 1, 1, TO_DATE('2020-06-01','yyyy-mm-dd'), TO_DATE('2020-06-03','yyyy-mm-dd'), 1, 1);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (1, 1,  TO_TIMESTAMP('2020-05-11 14:00:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (1, 2,  TO_TIMESTAMP('2020-06-01 17:30:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (1, 3,  TO_TIMESTAMP('2020-06-03 11:15:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (2, 222333444, 1, 1, TO_DATE('2020-09-01','yyyy-mm-dd'), TO_DATE('2020-09-05','yyyy-mm-dd'), 1, 3);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (2, 1,  TO_TIMESTAMP('2020-08-01 09:28:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (2, 2,  TO_TIMESTAMP('2020-09-01 18:46:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (2, 3,  TO_TIMESTAMP('2020-09-05 10:06:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (3, 333444555, 2, 2, TO_DATE('2020-08-08','yyyy-mm-dd'), TO_DATE('2020-08-11','yyyy-mm-dd'), null, null);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (3, 1,  TO_TIMESTAMP('2020-08-01 13:09:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (3, 4,  TO_TIMESTAMP('2020-08-07 08:15:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (4, 444555666, 3, 2, TO_DATE('2020-07-05','yyyy-mm-dd'), TO_DATE('2020-07-10','yyyy-mm-dd'), 6, 3);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (4, 1,  TO_TIMESTAMP('2020-05-09 19:35:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (4, 2,  TO_TIMESTAMP('2020-07-05 14:45:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (4, 3,  TO_TIMESTAMP('2020-07-10 11:17:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (5, 111222333, 1, 1, TO_DATE('2021-08-01','yyyy-mm-dd'), TO_DATE('2021-08-05','yyyy-mm-dd'), null, null);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (5, 1,  TO_TIMESTAMP('2020-11-11 14:45:00', 'yyyy-mm-dd hh24:mi:ss'));

-- Reservas �poca M�dia
INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (6, 222333444, 3, 3, TO_DATE('2020-02-28','yyyy-mm-dd'), TO_DATE('2020-03-03','yyyy-mm-dd'), 25, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (6, 1,  TO_TIMESTAMP('2020-01-11 20:38:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (6, 2,  TO_TIMESTAMP('2020-02-28 15:20:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (6, 3,  TO_TIMESTAMP('2020-03-03 12:10:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (7, 333444555, 1, 1, TO_DATE('2020-05-23','yyyy-mm-dd'), TO_DATE('2020-05-27','yyyy-mm-dd'), 1, 3);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (7, 1,  TO_TIMESTAMP('2020-02-10 14:45:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (7, 2,  TO_TIMESTAMP('2020-05-23 17:40:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (7, 3,  TO_TIMESTAMP('2020-05-27 11:50:00', 'yyyy-mm-dd hh24:mi:ss'));


INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (8, 444555666, 1, 1, TO_DATE('2020-04-12','yyyy-mm-dd'), TO_DATE('2020-04-18','yyyy-mm-dd'), null, null);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (8, 1,  TO_TIMESTAMP('2020-03-01 14:45:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (8, 4,  TO_TIMESTAMP('2020-03-20 15:20:00', 'yyyy-mm-dd hh24:mi:ss'));


INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (9, 222333444, 1, 1, TO_DATE('2020-03-17','yyyy-mm-dd'), TO_DATE('2020-03-28','yyyy-mm-dd'), 1, 1);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (9, 1,  TO_TIMESTAMP('2020-03-02 07:45:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (9, 2,  TO_TIMESTAMP('2020-03-17 16:45:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (9, 3,  TO_TIMESTAMP('2020-03-28 10:08:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (10, 333444555, 1, 1, TO_DATE('2021-02-03','yyyy-mm-dd'), TO_DATE('2021-02-10','yyyy-mm-dd'), null, null);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (10, 1,  TO_TIMESTAMP('2020-11-11 14:45:00', 'yyyy-mm-dd hh24:mi:ss'));

-- Reservas �poca Baixa

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (11, 222333444, 1, 1, TO_DATE('2020-10-10','yyyy-mm-dd'), TO_DATE('2020-10-13','yyyy-mm-dd'), 2, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (11, 1,  TO_TIMESTAMP('2020-08-23 21:05:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (11, 2,  TO_TIMESTAMP('2020-10-10 18:00:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (11, 3,  TO_TIMESTAMP('2020-10-13 09:40:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (12, 111222333, 4, 2, TO_DATE('2020-11-08','yyyy-mm-dd'), TO_DATE('2020-11-15','yyyy-mm-dd'), 12, 3);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (12, 1,  TO_TIMESTAMP('2020-06-11 22:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (12, 2,  TO_TIMESTAMP('2020-11-08 13:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (12, 3,  TO_TIMESTAMP('2020-11-15 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (13, 222333444, 3, 2, TO_DATE('2020-12-29','yyyy-mm-dd'), TO_DATE('2021-01-05','yyyy-mm-dd'), null, null);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (13, 1,  TO_TIMESTAMP('2020-11-11 14:45:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (14, 333444555, 2, 2, TO_DATE('2020-12-29','yyyy-mm-dd'), TO_DATE('2021-01-03','yyyy-mm-dd'), null, null);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (14, 1,  TO_TIMESTAMP('2020-11-11 14:45:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (15, 555666777, 1, 1, TO_DATE('2020-12-22','yyyy-mm-dd'), TO_DATE('2020-12-26','yyyy-mm-dd'), null, null);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (15, 1,  TO_TIMESTAMP('2020-11-11 14:45:00', 'yyyy-mm-dd hh24:mi:ss'));

-- BEGIN -- Reservas para o Sr. José Silva
INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (16, 554785485, 1, 1, TO_DATE('2019-01-05','yyyy-mm-dd'), TO_DATE('2019-01-10','yyyy-mm-dd'), 1, 1);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (16, 1,  TO_TIMESTAMP('2019-01-01 17:35:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (16, 2,  TO_TIMESTAMP('2019-01-05 17:35:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (16, 3,  TO_TIMESTAMP('2019-01-10 09:35:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (17, 554785485, 1, 1, TO_DATE('2020-10-01','yyyy-mm-dd'), TO_DATE('2020-10-10','yyyy-mm-dd'), 2, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (17, 1,  TO_TIMESTAMP('2020-09-29 17:35:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (17, 2,  TO_TIMESTAMP('2020-10-01 19:00:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (17, 3,  TO_TIMESTAMP('2020-10-10 10:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (18, 554785485, 4, 1, TO_DATE('2020-05-05','yyyy-mm-dd'), TO_DATE('2020-05-10','yyyy-mm-dd'), 3, 3);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (18, 1,  TO_TIMESTAMP('2020-05-01 09:43:57', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (18, 2,  TO_TIMESTAMP('2020-05-05 21:35:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (18, 3,  TO_TIMESTAMP('2020-05-10 07:25:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (19, 555666777, 4, 2, TO_DATE('2020-11-01','yyyy-mm-dd'), TO_DATE('2020-11-02','yyyy-mm-dd'), 12, 3);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (19, 1,  TO_TIMESTAMP('2020-09-11 22:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (19, 2,  TO_TIMESTAMP('2020-11-01 13:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (19, 3,  TO_TIMESTAMP('2020-11-12 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (20, 444555666, 3, 2, TO_DATE('2020-10-05','yyyy-mm-dd'), TO_DATE('2020-10-06','yyyy-mm-dd'), 25, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (20, 1,  TO_TIMESTAMP('2020-09-09 19:35:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (20, 2,  TO_TIMESTAMP('2020-10-05 14:45:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (20, 3,  TO_TIMESTAMP('2020-10-06 11:17:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (21, 444555666, 4, 2, TO_DATE('2020-11-01','yyyy-mm-dd'), TO_DATE('2020-11-02','yyyy-mm-dd'), 12, 1);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (21, 1,  TO_TIMESTAMP('2020-09-01 22:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (21, 2,  TO_TIMESTAMP('2020-10-21 13:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (21, 3,  TO_TIMESTAMP('2020-10-23 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (22, 111222333, 4, 2, TO_DATE('2020-10-21','yyyy-mm-dd'), TO_DATE('2020-10-23','yyyy-mm-dd'), 12, 1);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (22, 1,  TO_TIMESTAMP('2020-09-01 22:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (22, 2,  TO_TIMESTAMP('2020-10-21 13:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (22, 3,  TO_TIMESTAMP('2020-10-23 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

-- END -- Reservas para o Sr. José Silva

-- Mais algumas reservas part 1
INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (23, 547854125, 2, 4, TO_DATE('2020-01-01','yyyy-mm-dd'), TO_DATE('2020-01-03','yyyy-mm-dd'), 11, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (23, 1,  TO_TIMESTAMP('2019-12-29 09:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (23, 2,  TO_TIMESTAMP('2020-01-01 13:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (23, 3,  TO_TIMESTAMP('2020-01-03 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (24, 547854125, 2, 2, TO_DATE('2020-02-01','yyyy-mm-dd'), TO_DATE('2020-02-19','yyyy-mm-dd'), 19, 3);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (24, 1,  TO_TIMESTAMP('2020-01-25 12:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (24, 2,  TO_TIMESTAMP('2020-02-01 19:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (24, 3,  TO_TIMESTAMP('2020-02-19 07:24:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (25, 333444555, 2, 3, TO_DATE('2020-03-10','yyyy-mm-dd'), TO_DATE('2020-03-13','yyyy-mm-dd'), 18, 1);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (25, 1,  TO_TIMESTAMP('2020-03-01 14:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (25, 2,  TO_TIMESTAMP('2020-03-10 17:27:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (25, 3,  TO_TIMESTAMP('2020-03-13 08:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (26, 333444555, 2, 2, TO_DATE('2020-03-11','yyyy-mm-dd'), TO_DATE('2020-03-15','yyyy-mm-dd'), 12, 1);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (26, 1,  TO_TIMESTAMP('2020-03-10 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (26, 2,  TO_TIMESTAMP('2020-03-11 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (26, 3,  TO_TIMESTAMP('2020-03-15 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

-- Reservas para consumos em �poca Alta
INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (27, 333444555, 4, 3, TO_DATE('2020-08-09','yyyy-mm-dd'), TO_DATE('2020-08-11','yyyy-mm-dd'), 31, 1);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (27, 1,  TO_TIMESTAMP('2020-08-01 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (27, 2,  TO_TIMESTAMP('2020-08-09 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (27, 3,  TO_TIMESTAMP('2020-08-11 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (28, 547854125, 4, 3, TO_DATE('2020-08-12','yyyy-mm-dd'), TO_DATE('2020-08-13','yyyy-mm-dd'), 31, 1);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (28, 1,  TO_TIMESTAMP('2020-08-01 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (28, 2,  TO_TIMESTAMP('2020-08-12 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (28, 3,  TO_TIMESTAMP('2020-08-13 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (29, 111222333, 4, 3, TO_DATE('2020-08-14','yyyy-mm-dd'), TO_DATE('2020-08-15','yyyy-mm-dd'), 31, 1);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (29, 1,  TO_TIMESTAMP('2020-08-01 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (29, 2,  TO_TIMESTAMP('2020-08-14 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (29, 3,  TO_TIMESTAMP('2020-08-15 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (30, 333444555, 2, 2, TO_DATE('2020-08-01','yyyy-mm-dd'), TO_DATE('2020-08-03','yyyy-mm-dd'), 12, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (27, 1,  TO_TIMESTAMP('2020-04-11 14:00:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (27, 2,  TO_TIMESTAMP('2020-08-01 17:30:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (27, 3,  TO_TIMESTAMP('2020-08-03 11:15:00', 'yyyy-mm-dd hh24:mi:ss'));

-- Mais algumas reservas part 2

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (31, 117854873, 1, 1, TO_DATE('2020-08-01','yyyy-mm-dd'), TO_DATE('2020-08-07','yyyy-mm-dd'), 10, 3);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (31, 1,  TO_TIMESTAMP('2020-07-05 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (31, 2,  TO_TIMESTAMP('2020-08-01 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (31, 3,  TO_TIMESTAMP('2020-08-07 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (32, 117854873, 2, 1, TO_DATE('2020-08-05','yyyy-mm-dd'), TO_DATE('2020-08-10','yyyy-mm-dd'), 20, 3);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (32, 1,  TO_TIMESTAMP('2020-08-01 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (32, 2,  TO_TIMESTAMP('2020-08-05 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (32, 3,  TO_TIMESTAMP('2020-08-10 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (33, 117854873, 3, 1, TO_DATE('2020-07-20','yyyy-mm-dd'), TO_DATE('2020-07-28','yyyy-mm-dd'), 25, 3);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (33, 1,  TO_TIMESTAMP('2020-07-01 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (33, 2,  TO_TIMESTAMP('2020-07-20 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (33, 3,  TO_TIMESTAMP('2020-07-28 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (34, 117854873, 1, 1, TO_DATE('2020-07-01','yyyy-mm-dd'), TO_DATE('2020-07-04','yyyy-mm-dd'), 8, 3);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (34, 1,  TO_TIMESTAMP('2020-06-29 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (34, 2,  TO_TIMESTAMP('2020-07-01 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (34, 3,  TO_TIMESTAMP('2020-07-04 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (35, 117854873, 2, 1, TO_DATE('2020-07-07','yyyy-mm-dd'), TO_DATE('2020-07-12','yyyy-mm-dd'), 13, 3);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (35, 1,  TO_TIMESTAMP('2020-07-05 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (35, 2,  TO_TIMESTAMP('2020-07-07 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (35, 3,  TO_TIMESTAMP('2020-07-12 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (36, 117854873, 3, 1, TO_DATE('2020-06-15','yyyy-mm-dd'), TO_DATE('2020-06-21','yyyy-mm-dd'), 24, 3);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (36, 1,  TO_TIMESTAMP('2020-06-07 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (36, 2,  TO_TIMESTAMP('2020-06-15 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (36, 3,  TO_TIMESTAMP('2020-06-21 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (37, 111222333, 3, 1, TO_DATE('2020-11-01','yyyy-mm-dd'), TO_DATE('2020-11-05','yyyy-mm-dd'), 24, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (37, 1,  TO_TIMESTAMP('2020-10-29 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (37, 2,  TO_TIMESTAMP('2020-11-01 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (37, 3,  TO_TIMESTAMP('2020-11-05 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (38, 222333444, 1, 1, TO_DATE('2020-11-03','yyyy-mm-dd'), TO_DATE('2020-11-07','yyyy-mm-dd'), 9, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (38, 1,  TO_TIMESTAMP('2020-10-29 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (38, 2,  TO_TIMESTAMP('2020-11-03 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (38, 3,  TO_TIMESTAMP('2020-11-07 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (39, 111222333, 2, 2, TO_DATE('2020-05-01','yyyy-mm-dd'), TO_DATE('2020-05-05','yyyy-mm-dd'), 17, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (39, 1,  TO_TIMESTAMP('2020-05-01 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (39, 2,  TO_TIMESTAMP('2020-05-01 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (39, 3,  TO_TIMESTAMP('2020-05-05 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (40, 222333444, 3, 1, TO_DATE('2020-05-25','yyyy-mm-dd'), TO_DATE('2020-05-30','yyyy-mm-dd'), 23, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (40, 1,  TO_TIMESTAMP('2020-05-03 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (40, 2,  TO_TIMESTAMP('2020-05-25 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (40, 3,  TO_TIMESTAMP('2020-05-30 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (41, 111222333, 1, 2, TO_DATE('2020-09-01','yyyy-mm-dd'), TO_DATE('2020-09-04','yyyy-mm-dd'), 1, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (41, 1,  TO_TIMESTAMP('2020-08-29 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (41, 2,  TO_TIMESTAMP('2020-09-01 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (41, 3,  TO_TIMESTAMP('2020-09-04 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (42, 222333444, 2, 2, TO_DATE('2020-09-23','yyyy-mm-dd'), TO_DATE('2020-09-27','yyyy-mm-dd'), 11, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (42, 1,  TO_TIMESTAMP('2020-09-10 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (42, 2,  TO_TIMESTAMP('2020-09-23 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (42, 3,  TO_TIMESTAMP('2020-09-27 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (43, 111222333, 2, 1, TO_DATE('2020-08-15','yyyy-mm-dd'), TO_DATE('2020-08-20','yyyy-mm-dd'), 19, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (43, 1,  TO_TIMESTAMP('2020-08-01 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (43, 2,  TO_TIMESTAMP('2020-08-15 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (43, 3,  TO_TIMESTAMP('2020-08-20 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

INSERT INTO Reserva(id_reserva, nif, id_tipo_quarto, numero_pessoas, data_inicio, data_fim, numero_quarto, numero_andar) 
VALUES (44, 222333444, 1, 1, TO_DATE('2020-08-15','yyyy-mm-dd'), TO_DATE('2020-08-23','yyyy-mm-dd'), 7, 2);
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (44, 1,  TO_TIMESTAMP('2020-08-01 18:13:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (44, 2,  TO_TIMESTAMP('2020-08-15 18:57:00', 'yyyy-mm-dd hh24:mi:ss'));
INSERT INTO EstadoReserva(id_reserva, id_estado, data) VALUES (44, 3,  TO_TIMESTAMP('2020-08-23 09:30:00', 'yyyy-mm-dd hh24:mi:ss'));

-- INSERT INTO PedidoIntervencao(id_pedido_intervencao, data_pedido_intervencao, id_reserva) VALUES (?, ?, ?);
INSERT INTO PedidoIntervencao(id_pedido_intervencao, data_pedido_intervencao, id_reserva) VALUES (1, TO_DATE('2020-06-02','yyyy-mm-dd'), 1);
INSERT INTO PedidoIntervencao(id_pedido_intervencao, data_pedido_intervencao, id_reserva) VALUES (2, TO_DATE('2020-07-08','yyyy-mm-dd'), 4);
INSERT INTO PedidoIntervencao(id_pedido_intervencao, data_pedido_intervencao, id_reserva) VALUES (3, TO_DATE('2020-03-23','yyyy-mm-dd'), 9);
INSERT INTO PedidoIntervencao(id_pedido_intervencao, data_pedido_intervencao, id_reserva) VALUES (4, TO_DATE('2020-11-10','yyyy-mm-dd'), 12);
INSERT INTO PedidoIntervencao(id_pedido_intervencao, data_pedido_intervencao, id_reserva) VALUES (5, TO_DATE('2020-11-12','yyyy-mm-dd'), 12);
INSERT INTO PedidoIntervencao(id_pedido_intervencao, data_pedido_intervencao, id_reserva) VALUES (6, TO_DATE('2020-09-30','yyyy-mm-dd'), 17);

-- INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) VALUES (?, ?, ?, ?);
-- Registos de Interven��es de limpeza
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (1, TO_TIMESTAMP('2020-06-01 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (2, TO_TIMESTAMP('2020-06-02 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (3, TO_TIMESTAMP('2020-06-03 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (4, TO_TIMESTAMP('2020-09-01 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (5, TO_TIMESTAMP('2020-09-02 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (6, TO_TIMESTAMP('2020-09-03 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (7, TO_TIMESTAMP('2020-09-04 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (8, TO_TIMESTAMP('2020-09-05 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (9, TO_TIMESTAMP('2020-07-05 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 6, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (10, TO_TIMESTAMP('2020-07-06 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 6, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (11, TO_TIMESTAMP('2020-07-07 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 6, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (12, TO_TIMESTAMP('2020-07-08 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 6, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (13, TO_TIMESTAMP('2020-07-09 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 6, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (14, TO_TIMESTAMP('2020-07-10 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 6, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (15, TO_TIMESTAMP('2020-02-28 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 4, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (16, TO_TIMESTAMP('2020-02-29 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 4, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (17, TO_TIMESTAMP('2020-03-01 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 4, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (18, TO_TIMESTAMP('2020-03-02 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 4, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (19, TO_TIMESTAMP('2020-03-03 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 4, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (20, TO_TIMESTAMP('2020-05-23 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (21, TO_TIMESTAMP('2020-05-24 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (22, TO_TIMESTAMP('2020-05-25 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (23, TO_TIMESTAMP('2020-05-26 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (24, TO_TIMESTAMP('2020-05-27 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (25, TO_TIMESTAMP('2020-03-17 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (26, TO_TIMESTAMP('2020-03-18 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (27, TO_TIMESTAMP('2020-03-19 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (28, TO_TIMESTAMP('2020-03-20 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (29, TO_TIMESTAMP('2020-03-21 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (30, TO_TIMESTAMP('2020-03-22 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (31, TO_TIMESTAMP('2020-03-23 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (32, TO_TIMESTAMP('2020-03-24 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (33, TO_TIMESTAMP('2020-03-25 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (34, TO_TIMESTAMP('2020-03-26 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (35, TO_TIMESTAMP('2020-03-27 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (36, TO_TIMESTAMP('2020-03-28 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (37, TO_TIMESTAMP('2020-10-10 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 2, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (38, TO_TIMESTAMP('2020-10-11 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 2, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (39, TO_TIMESTAMP('2020-10-12 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 2, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (40, TO_TIMESTAMP('2020-10-13 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 2, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (41, TO_TIMESTAMP('2020-11-08 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 9, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (42, TO_TIMESTAMP('2020-11-09 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 9, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (43, TO_TIMESTAMP('2020-11-10 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 9, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (44, TO_TIMESTAMP('2020-11-11 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 9, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (45, TO_TIMESTAMP('2020-11-12 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 9, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (46, TO_TIMESTAMP('2020-11-13 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 9, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (47, TO_TIMESTAMP('2020-11-14 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 9, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (48, TO_TIMESTAMP('2020-11-15 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 9, 3);

INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (49, null, 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (50, TO_TIMESTAMP('2020-11-15 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 6, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (51, TO_TIMESTAMP('2020-11-15 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (52, null, 12, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (53, null, 12, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (54, null, 2, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (55, TO_TIMESTAMP('2020-08-10 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 31, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (56, TO_TIMESTAMP('2020-08-13 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 31, 1);

--Adionar mais alguns registos RegistoIntervencao para limpeza
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (57, TO_TIMESTAMP('2020-08-02 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 10, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (58, TO_TIMESTAMP('2020-08-04 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 10, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (59, TO_TIMESTAMP('2020-08-06 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 10, 1);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (60, TO_TIMESTAMP('2020-08-06 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 20, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (61, TO_TIMESTAMP('2020-08-07 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 20, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (62, TO_TIMESTAMP('2020-07-22 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 25, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (63, TO_TIMESTAMP('2020-07-24 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 25, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (64, TO_TIMESTAMP('2020-07-26 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 25, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (65, TO_TIMESTAMP('2020-07-02 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 8, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (66, TO_TIMESTAMP('2020-07-03 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 8, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (67, TO_TIMESTAMP('2020-07-09 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 13, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (68, TO_TIMESTAMP('2020-06-19 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 24, 3);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (69, TO_TIMESTAMP('2020-11-02 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 24, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (70, TO_TIMESTAMP('2020-11-04 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 24, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (71, TO_TIMESTAMP('2020-11-04 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 9, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (72, TO_TIMESTAMP('2020-11-06 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 9, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (73, TO_TIMESTAMP('2020-05-02 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 17, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (74, TO_TIMESTAMP('2020-05-04 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 17, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (75, TO_TIMESTAMP('2020-05-26 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 23, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (76, TO_TIMESTAMP('2020-05-29 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 23, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (77, TO_TIMESTAMP('2020-09-02 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar)
VALUES (78, TO_TIMESTAMP('2020-09-03 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (79, TO_TIMESTAMP('2020-09-24 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 11, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (80, TO_TIMESTAMP('2020-09-26 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 11, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (81, TO_TIMESTAMP('2020-08-17 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 19, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (82, TO_TIMESTAMP('2020-08-19 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 19, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (83, TO_TIMESTAMP('2020-08-18 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 7, 2);
INSERT INTO RegistoIntervencao(id_intervencao, data_intervencao, numero_quarto, numero_andar) 
VALUES (84, TO_TIMESTAMP('2020-08-20 09:00:00', 'yyyy-mm-dd hh24:mi:ss'), 7, 2);

-- INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (?, ?);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (1, 888999000);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (2, 999000111);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (3, 999000111);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (4, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (5, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (6, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (7, 999000111);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (8, 999000111);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (9, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (10, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (11, 999000111);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (12, 888999000);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (13, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (14, 567123098);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (15, 567123098);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (16, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (17, 888999000);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (18, 888999000);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (19, 999000111);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (20, 888999000);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (21, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (22, 567123098);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (23, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (24, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (25, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (26, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (27, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (28, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (29, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (30, 999000111);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (31, 888999000);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (32, 999000111);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (33, 999000111);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (34, 999000111);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (35, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (36, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (37, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (38, 888999000);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (39, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (40, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (41, 567123098);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (42, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (43, 888999000);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (44, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (45, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (46, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (47, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (48, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (55, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (56, 888999000);
--Adionar mais alguns registos RegistoIntervencao para limpeza
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (57, 567123098);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (58, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (59, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (60, 567123098);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (61, 888999000);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (62, 567123098);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (63, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (64, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (65, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (66, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (67, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (68, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (69, 888999000);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (70, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (71, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (72, 567123098);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (73, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (74, 888999000);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (75, 567123098);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (76, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (77, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (78, 567123098);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (79, 888999000);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (80, 234890567);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (81, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (82, 567123098);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (83, 123456789);
INSERT INTO IntervencaoLimpeza(id_intervencao, nif_funcionario) VALUES (84, 234890567);

-- INSERT INTO IntervencaoManutencao(id_intervencao, nif_funcionario, descricao, id_pedido_intervencao) VALUES (?, ?, ?, ?);
INSERT INTO IntervencaoManutencao(id_intervencao, nif_funcionario, descricao, id_pedido_intervencao) VALUES (49, 777888999, 'Tomada TV não funciona', 1);
INSERT INTO IntervencaoManutencao(id_intervencao, nif_funcionario, descricao, id_pedido_intervencao) VALUES (50, 666777888, 'AC não aquece', 2);
INSERT INTO IntervencaoManutencao(id_intervencao, nif_funcionario, descricao, id_pedido_intervencao) VALUES (51, 666777888, 'Autoclismo não funciona', 3);
INSERT INTO IntervencaoManutencao(id_intervencao, nif_funcionario, descricao, id_pedido_intervencao) VALUES (52, 777888999, 'Estore não abre', 4);
INSERT INTO IntervencaoManutencao(id_intervencao, nif_funcionario, descricao, id_pedido_intervencao) VALUES (53, 777888999, 'Telefone com problemas', 5);
INSERT INTO IntervencaoManutencao(id_intervencao, nif_funcionario, descricao, id_pedido_intervencao) VALUES (54, 666777888, 'Torneira partida', 6);

-- INSERT INTO TipoProduto(id_tipo_produto, descricao_tipo) VALUES (?, ?);
INSERT INTO TipoProduto(id_tipo_produto, descricao_tipo) VALUES (1, 'Bebida');
INSERT INTO TipoProduto(id_tipo_produto, descricao_tipo) VALUES (2, 'Sack');

-- INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (?, ?, ?, ?);
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (1, 'Snickers 50g', 2, 1.10);						--snack
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (2, 'Vitalis 0,5L', 1, 1.00);						--bebida
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (3, 'Coca-Cola 0,3L', 1, 1.50);					--bebida
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (4, 'Amendoins no Forno 200g', 2, 1.50);			--snack
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (5, 'Haribo 100g', 2, 1.20);						--snack
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (6, 'Vinho do Porto Cruz 0,05L', 1, 1.75);		--bebida
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (7, 'Licor Beir�o 0,05L', 1, 1.10);				--bebida
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (8, 'Pipocas Caramelizadas 150g', 2, 1.40);		--snack
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (9, 'Barra de Cereais Choco 50g', 2, 1.40);		--snack
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (10, 'Rum Bacardi 0,05L', 1, 1.25);				--bebida
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (11, 'Jagermeister 0,05L', 1, 1.30);				--bebida
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (12, 'Bolacha Belga Choco 200g', 2, 2.20);		--snack
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (13, 'Batata Frita Ketchup 160g', 2, 1.30);		--snack
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (14, 'Red Bull 0,25L', 1, 1.50);					--bebida
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (15, 'Mateus Ros� 0,25L', 1, 1.50);				--bebida
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (16, 'Champagne Moet Chandon 0,2L', 1, 15.00);	--bebida
INSERT INTO Produto(id_produto, descricao_produto, id_tipo_produto, preco) VALUES (17, '7UP 0,3L', 1, 1.50);						--bebida


-- INSERT INTO ContaConsumo(data_abertura, id_conta, id_reserva) VALUES (?, ?, ?);
INSERT INTO ContaConsumo(data_hora_abertura, id_conta, id_reserva) VALUES (TO_TIMESTAMP('2020-06-02 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1);
INSERT INTO ContaConsumo(data_hora_abertura, id_conta, id_reserva) VALUES (TO_TIMESTAMP('2020-09-02 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 2, 2);
INSERT INTO ContaConsumo(data_hora_abertura, id_conta, id_reserva) VALUES (TO_TIMESTAMP('2020-07-06 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 3, 4);
INSERT INTO ContaConsumo(data_hora_abertura, id_conta, id_reserva) VALUES (TO_TIMESTAMP('2020-02-29 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 4, 6);
INSERT INTO ContaConsumo(data_hora_abertura, id_conta, id_reserva) VALUES (TO_TIMESTAMP('2020-05-24 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 5, 7);
INSERT INTO ContaConsumo(data_hora_abertura, id_conta, id_reserva) VALUES (TO_TIMESTAMP('2020-03-18 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 6, 9);
INSERT INTO ContaConsumo(data_hora_abertura, id_conta, id_reserva) VALUES (TO_TIMESTAMP('2020-10-11 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 7, 11);
INSERT INTO ContaConsumo(data_hora_abertura, id_conta, id_reserva) VALUES (TO_TIMESTAMP('2020-11-09 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 8, 12);
INSERT INTO ContaConsumo(data_hora_abertura, id_conta, id_reserva) VALUES (TO_TIMESTAMP('2020-08-10 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 9, 27);
INSERT INTO ContaConsumo(data_hora_abertura, id_conta, id_reserva) VALUES (TO_TIMESTAMP('2020-08-10 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 10, 28);

-- INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) VALUES (?, ?, ?, ?, ?);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (1, TO_TIMESTAMP('2020-06-02 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1, 2);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (1, TO_TIMESTAMP('2020-06-02 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 3, 1, 2);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (2, TO_TIMESTAMP('2020-09-02 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 8, 1, 5);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao)
VALUES (2, TO_TIMESTAMP('2020-09-02 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 17, 1, 5);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (2, TO_TIMESTAMP('2020-09-03 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 13, 1, 6);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (2, TO_TIMESTAMP('2020-09-03 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 10, 1, 6);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (2, TO_TIMESTAMP('2020-09-04 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 4, 1, 7);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (2, TO_TIMESTAMP('2020-09-04 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 15, 1, 7);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (3, TO_TIMESTAMP('2020-07-06 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 5, 1, 10);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (3, TO_TIMESTAMP('2020-07-06 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 2, 1, 10);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (3, TO_TIMESTAMP('2020-07-07 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 9, 1, 11);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (3, TO_TIMESTAMP('2020-07-07 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 2, 1, 11);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (3, TO_TIMESTAMP('2020-07-08 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 12, 1, 12);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (3, TO_TIMESTAMP('2020-07-08 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 2, 1, 12);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (3, TO_TIMESTAMP('2020-07-09 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 8, 1, 13);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (3, TO_TIMESTAMP('2020-07-09 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 2, 1, 13);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (4, TO_TIMESTAMP('2020-02-29 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 6, 1, 16);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (4, TO_TIMESTAMP('2020-03-01 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 11, 1, 17);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (4, TO_TIMESTAMP('2020-03-02 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 16, 1, 18);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (5, TO_TIMESTAMP('2020-05-24 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 4, 1, 21);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (5, TO_TIMESTAMP('2020-05-24 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 7, 1, 21);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (5, TO_TIMESTAMP('2020-05-25 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 9, 1, 22);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (5, TO_TIMESTAMP('2020-05-25 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 17, 1, 22);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (5, TO_TIMESTAMP('2020-05-26 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1, 23);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (5, TO_TIMESTAMP('2020-05-26 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 10, 1, 23);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (6, TO_TIMESTAMP('2020-03-18 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 3, 1, 26);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (6, TO_TIMESTAMP('2020-03-20 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 14, 1, 28);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (6, TO_TIMESTAMP('2020-03-20 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 12, 1, 28);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (6, TO_TIMESTAMP('2020-03-22 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 3, 1, 30);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (6, TO_TIMESTAMP('2020-03-22 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 1, 1, 30);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (6, TO_TIMESTAMP('2020-03-24 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 4, 1, 32);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (6, TO_TIMESTAMP('2020-03-24 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 6, 1, 32);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (6, TO_TIMESTAMP('2020-03-26 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 7, 1, 34);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (7, TO_TIMESTAMP('2020-10-11 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 5, 1, 38);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (7, TO_TIMESTAMP('2020-10-11 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 2, 1, 38);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (7, TO_TIMESTAMP('2020-10-12 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 8, 1, 39);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (7, TO_TIMESTAMP('2020-10-12 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 3, 1, 39);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (8, TO_TIMESTAMP('2020-11-09 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 16, 1, 42);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (8, TO_TIMESTAMP('2020-11-10 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 15, 1, 43);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (8, TO_TIMESTAMP('2020-11-11 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 16, 1, 44);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (8, TO_TIMESTAMP('2020-11-12 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 10, 1, 45);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao)
VALUES (8, TO_TIMESTAMP('2020-11-13 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 7, 1, 46);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (8, TO_TIMESTAMP('2020-11-14 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 16, 1, 47);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (9, TO_TIMESTAMP('2020-08-10 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 3, 2, 48);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (9, TO_TIMESTAMP('2020-08-10 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 2, 1, 48);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (9, TO_TIMESTAMP('2020-08-10 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 17, 1, 48);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (10, TO_TIMESTAMP('2020-08-13 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 3, 5, 55);
INSERT INTO Consumo(id_conta, data_consumo, id_produto, quantidade, id_intervencao) 
VALUES (10, TO_TIMESTAMP('2020-08-13 12:00:00', 'yyyy-mm-dd hh24:mi:ss'), 2, 2, 55);


-- INSERT INTO MetodoPagamento(id_metodo_pagamento, descricao) VALUES (?, ?);
INSERT INTO MetodoPagamento(id_metodo_pagamento, descricao) VALUES (1, 'Cheque');
INSERT INTO MetodoPagamento(id_metodo_pagamento, descricao) VALUES (2, 'Cartão Crédito');
INSERT INTO MetodoPagamento(id_metodo_pagamento, descricao) VALUES (3, 'Cartão Débito');

-- INSERT INTO Fatura(id_fatura, valor_total, id_fatura_consumo, id_fatura_reserva) VALUES (?, ?, ?, ?);

-- INSERT INTO Fatura_MetodoPagamento(id_fatura, id_metodo_pagamento, valor_pago) VALUES (?, ?, ?);

-- INSERT INTO FaturaConsumo(id_fatura_consumo, id_conta, valor) VALUES (?, ?, ?);

-- INSERT INTO FaturaReserva(id_fatura_reserva, id_reserva, valor) VALUES (?, ?, ?);